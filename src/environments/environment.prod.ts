export const environment = {
  production: true,
  apiUrl: 'http://134.122.48.238:8080/api/',
  wsUrl: 'ws://134.122.48.238:8080/ws/'
};
