export const GENDERS = [
  {name: 'Male', uuid: 1},
  {name: 'Female', uuid: 2}
];

export const HIGHER_STATUSES = [
  {name: 'Graduated', uuid: 1},
  {name: 'Studying', uuid: 2}
];

export const VIEW_OPTIONS = [
  {name: 'Negative', uuid: 0},
  {name: 'Neutral', uuid: 1},
  {name: 'Positive', uuid: 2}
];

export const URL_VALIDATION_PATTERN = '[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)';
export const DATE_VALIDATION_PATTERN = '^[0-9]{8}$';
export const REGEX_TO_ADD_HTTP = new RegExp('^(ftp|http|https)://');

export const HIGHER_ENUM = {
  1: 'Graduated',
  2: 'Studying'
};

export const VIEW_ENUM = {
  0: 'Negative',
  1: 'Neutral',
  2: 'Positive'
};

export const NO_AVATAR = '/assets/images/main-no-avatar.png';
export const NO_AVATAR_SMALL = '/assets/images/no-avatar-circle.png';

export const DATEPICKER_STEPS = {
  DAY: 0,
  MONTH: 1,
  YEAR: 2
};

export const YEAR_MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

export const NOTE_FILTER_OPTIONS = [
  {name: 'All notes', uuid: 'all'},
  {name: 'Pending', uuid: 'pending'},
  {name: 'Mine', uuid: 'mine'},
  {name: 'Answered', uuid: 'answered'}
];
export const NOTE_SORT_OPTIONS = [
  {name: 'Recent', uuid: 'recent'},
  {name: 'Earlier', uuid: 'early'}
];

export const FILE_EXTENSIONS = {
  doc: 'doc.png',
  docx: 'doc.png',
  jpg: 'jpg.png',
  jpeg: 'jpg.png',
  pdf: 'pdf.png',
  png: 'png.png',
  ppt: 'ppt.png',
  pptx: 'ppt.png',
  xls: 'xls.png',
  xlsx: 'xls.png',
  zip: 'zip.png'
};

export const IMAGE_EXTENSIONS = ['jpg', 'png', 'gif', 'jpeg', 'tiff'];
export const VIDEO_EXTENSIONS = ['mp4', 'm4a', 'm4v', 'mov', 'ogg', 'ogv', 'webm', 'avi', 'flv'];
export const PAGINATION_LIMIT = 12;

export const DATEPICKER_TOGGLE_RANGE = 395;
export const URGENCY_NAMES = ['No priority', 'Not urgent', 'Urgently', 'Very urgent'];
