import {Injectable} from '@angular/core';
import {SnackbarData} from '../shared/interfaces/snackbar-data';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiWindowService {
  snackBar: Subject<SnackbarData> = new Subject<SnackbarData>();

  constructor() {}

  showSnackBar(data: SnackbarData): void {
    this.snackBar.next(data);
  }

  getSnackBar(): Observable<SnackbarData> {
    return this.snackBar.asObservable();
  }
}
