import {Injectable} from '@angular/core';
import {CustomHttpService} from './custom-http.service';
import {Observable} from 'rxjs';
import {City} from '../shared/models/city';
import {Country} from '../shared/models/country';
import {UserProfile} from '../shared/interfaces/user-profile';
import {UserBaseInfo} from '../shared/interfaces/user-base-info';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(
    private cHttp: CustomHttpService
  ) {}

  searchCities(word: string, country?: string): Observable<Array<City>> {
    const data: any = {name__startswith: word};
    if (country) {
      data.country__uuid = country;
    }
    return this.cHttp.get('locations/cities/', data);
  }

  searchCountries(word: string): Observable<Array<Country>> {
    return this.cHttp.get('locations/countries/', {name__startswith: word});
  }

  searchUsers(word: string): Observable<Array<UserProfile>> {
    return this.cHttp.get('users/', {user: word});
  }

  searchFriends(word: string): Observable<Array<UserBaseInfo>> {
    return this.cHttp.get('users/friends/my/', {friend: word});
  }
}
