import {Injectable} from '@angular/core';
import {Reviewer} from '../shared/interfaces/reviewer';
import {Observable} from 'rxjs';
import {CustomHttpService} from './custom-http.service';
import {Advantage} from '../shared/interfaces/advantage';

@Injectable({
  providedIn: 'root'
})
export class LandingDataService {
  constructor(
    private cHttp: CustomHttpService
  ) { }

  getReviewers(): Observable<Array<Reviewer>> {
    return this.cHttp.getFullUri('/assets/data/reviewers.json');
  }

  getAdvantages(): Observable<Array<Advantage>> {
    return this.cHttp.getFullUri('/assets/data/advantages.json');
  }
}
