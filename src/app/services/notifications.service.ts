import {Injectable, OnDestroy} from '@angular/core';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {environment} from '../../environments/environment';
import {UserService} from './user.service';
import {delay, retryWhen, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {NotificationSocket} from '../shared/interfaces/notification-socket';
import {CustomHttpService} from './custom-http.service';
import {Notification} from '../shared/interfaces/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService implements OnDestroy {
  connection: WebSocketSubject<any>;
  notificationCount: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  destroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  constructor(
    private userService: UserService,
    private cHttp: CustomHttpService
  ) {
    this.connect();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
    if (this.connection) {
      this.connection.unsubscribe();
    }
  }

  connect(): void {
    this.userService.userProfile.
      pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        if (data) {
          this.connection = webSocket({
            url: `${environment.wsUrl}notifications/${data.phone}/`
          });
          this.connection
            .pipe(retryWhen((errors) => errors.pipe(delay(2000))))
            .subscribe((data: NotificationSocket) => {
              this.notificationCount.next(data.notifications_count);
            });
        }
      });
  }

  readAll(): void {
    this.connection.next(true);
  }

  // HTTP queries

  getNotifications(): Observable<Array<Notification>> {
    return this.cHttp.get('notices/');
  }
}
