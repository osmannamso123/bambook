import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomHttpService} from './custom-http.service';
import {Observable} from 'rxjs';
import {BasicInfo} from '../shared/interfaces/basic-info';
import {ContactInfo} from '../shared/interfaces/contact-info';
import {DATE_VALIDATION_PATTERN, URL_VALIDATION_PATTERN} from '../values/values';
import {Interest} from '../shared/interfaces/interest';
import {SecondaryEducation} from '../shared/interfaces/secondary-education';
import {HigherEducation} from '../shared/interfaces/higher-education';
import {Career} from '../shared/interfaces/career';
import {MilitaryService} from '../shared/interfaces/military-service';
import {LifePosition} from '../shared/interfaces/life-position';
import {Portfolio} from '../shared/interfaces/portfolio';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  constructor(
    private fb: FormBuilder,
    private cHttp: CustomHttpService
  ) {}

  getBasicInfoForm(info?: BasicInfo): FormGroup {
    const is = !!info;
    return this.fb.group({
      first_name: [is ? info.first_name : '', Validators.required],
      last_name: [is ? info.last_name : '', Validators.required],
      username: [is ? info.username : ''],
      gender: [is ? info.gender : ''],
      birth_date: [is ? info.birth_date : '', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
      position: [is ? info.position : '']
    }, {updateOn: 'blur'});
  }

  getContactInfoForm(info?: ContactInfo): FormGroup {
    const is = !!info;
    return this.fb.group({
      country: [is && info.country ? info.country.uuid : ''],
      city: [is && info.city ? info.city.uuid : ''],
      alt_phone: [is ? info.alt_phone : ''],
      skype: [is ? info.skype : ''],
      website: [is ? info.website : '', Validators.pattern(URL_VALIDATION_PATTERN)],
      instagram: [is ? info.instagram : ''],
      facebook: [is ? info.facebook : ''],
      email: [is ? info.email : '', Validators.email]
    }, {updateOn: 'blur'});
  }

  getInterestsForm(info?: Interest): FormGroup {
    const is = !!info;
    return this.fb.group({
      activities: [is ? info.activities : ''],
      interests: [is ? info.interests : ''],
      favorite_musics: [is ? info.favorite_musics : ''],
      favorite_movies: [is ? info.favorite_movies : ''],
      favorite_books: [is ? info.favorite_books : ''],
      favorite_quotes: [is ? info.favorite_quotes : ''],
      about_me: [is ? info.about_me : '']
    }, {updateOn: 'blur'});
  }

  getSecondaryEducationForm(infos?: Array<SecondaryEducation>): FormArray {
    if (infos && infos.length) {
      return this.fb.array(infos.map((info) => {
        return this.fb.group({
          uuid: info.uuid || null,
          country: [info.specialization?.school?.country?.uuid || ''],
          city: [info.specialization?.school?.city?.uuid || ''],
          school: [info.specialization?.school?.name || '', Validators.required],
          issue_date: [info.issue_date, [Validators.pattern(DATE_VALIDATION_PATTERN)]],
          specialization_name: [info.specialization?.name || '']
        }, {updateOn: 'blur'});
      }));
    } else {
      return this.fb.array([this.fb.group({
        country: [''],
        city: [''],
        school: ['', Validators.required],
        issue_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
        specialization_name: ['']
    }, {updateOn: 'blur'})]);
    }
  }

  addSecondaryEducationForm(): FormGroup {
    return this.fb.group({
      country: [''],
      city: [''],
      school: ['', Validators.required],
      issue_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
      specialization_name: ['']
    }, {updateOn: 'blur'});
  }

  getHigherEducationForm(infos?: Array<HigherEducation>): FormArray {
    if (infos && infos.length) {
      return this.fb.array(infos.map((info) => {
        return this.fb.group({
          uuid: info.uuid,
          country: [info.faculty?.institution?.country?.uuid || ''],
          city: [info.faculty?.institution?.city?.uuid || ''],
          institution: [info.faculty?.institution?.institution, Validators.required],
          faculty_name: [info.faculty?.name],
          form_training: [info.form_training],
          status: [info.status],
          issue_date: [info.issue_date, [Validators.pattern(DATE_VALIDATION_PATTERN)]]
        }, {updateOn: 'blur'});
      }));
    } else {
      return this.fb.array([this.fb.group({
        country: [''],
        city: [''],
        institution: ['', Validators.required],
        faculty_name: [''],
        form_training: [''],
        status: [''],
        issue_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]]
      }, {updateOn: 'blur'})]);
    }
  }

  addHigherEducationForm(): FormGroup {
    return this.fb.group({
      country: [''],
      city: [''],
      institution: ['', Validators.required],
      faculty_name: [''],
      form_training: [''],
      status: [''],
      issue_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]]
    }, {updateOn: 'blur'});
  }

  getCareerForm(infos?: Array<Career>): FormArray {
    if (infos.length) {
      return this.fb.array(infos.map((info) => {
        return this.fb.group({
          uuid: info.uuid,
          company: [info.job?.name, Validators.required],
          country: [info.job?.country?.uuid],
          city: [info.job?.city?.uuid],
          from_date: [info.from_date, [Validators.pattern(DATE_VALIDATION_PATTERN)]],
          to_date: [info.to_date, [Validators.pattern(DATE_VALIDATION_PATTERN)]],
          position: [info.position]
        }, {updateOn: 'blur'});
      }));
    } else {
      return this.fb.array([this.fb.group({
        company: ['', Validators.required],
        country: [''],
        city: [''],
        from_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
        to_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
        position: ['']
      }, {updateOn: 'blur'})]);
    }
  }

  addCareerForm(): FormGroup {
    return this.fb.group({
      company: ['', Validators.required],
      country: [''],
      city: [''],
      from_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
      to_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
      position: ['']
    }, {updateOn: 'blur'});
  }

  getMilitaryForm(infos?: Array<MilitaryService>): FormArray {
    if (infos && infos.length) {
      return this.fb.array(infos.map((info) => {
        return this.fb.group({
          uuid: info.uuid,
          country: [info.army_part?.country?.uuid],
          army_part: [info.army_part?.name, Validators.required],
          from_date: [info.from_date, [Validators.pattern(DATE_VALIDATION_PATTERN)]],
          to_date: [info.to_date, [Validators.pattern(DATE_VALIDATION_PATTERN)]]
        }, {updateOn: 'blur'});
      }));
    } else {
      return this.fb.array([this.fb.group({
        country: [''],
        army_part: ['', Validators.required],
        from_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
        to_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]]
      }, {updateOn: 'blur'})]);
    }
  }

  addMilitaryForm(): FormGroup {
    return this.fb.group({
      country: [''],
      army_part: ['', Validators.required],
      from_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]],
      to_date: ['', [Validators.pattern(DATE_VALIDATION_PATTERN)]]
    }, {updateOn: 'blur'});
  }

  getLifePositionForm(info?): FormGroup {
    const is = !!info;
    return this.fb.group({
      political_views: [is ? info.political_views : null],
      world_views: [is ? info.world_views : null],
      personal_priority: [is ? info.personal_priority : null],
      important_in_others: [is ? info.important_in_others : null],
      smoking_views: [is ? info.smoking_views : null],
      alcohol_views: [is ? info.alcohol_views : null],
      inspired_by: [is ? info.inspired_by : null]
    }, {updateOn: 'blur'});
  }

  getPortfolioForm(infos?: Array<Portfolio>): FormArray {
    if (infos && infos.length) {
      return this.fb.array(infos.map((info) => {
        return this.fb.group({
          uuid: info.uuid,
          url: [info.url, Validators.pattern(URL_VALIDATION_PATTERN)]
        }, {updateOn: 'blur'});
      }));
    } else {
      return this.fb.array([this.fb.group({
        url: ['', [Validators.required, Validators.pattern(URL_VALIDATION_PATTERN)]]
      }, {updateOn: 'blur'})]);
    }
  }

  addPortfolioForm(): FormGroup {
    return this.fb.group({
      url: ['', [Validators.required, Validators.pattern(URL_VALIDATION_PATTERN)]]
    }, {updateOn: 'blur'});
  }

  // HTTP queries

  getBasicInfo(): Observable<BasicInfo> {
    return this.cHttp.get('users/update/');
  }

  saveBasicInfo(data: any): Observable<any> {
    return this.cHttp.put('users/update/', data);
  }

  getContactInfo(): Observable<ContactInfo> {
    return this.cHttp.get('users/update/contacts/');
  }

  saveContactInfo(data: any): Observable<any> {
    return this.cHttp.put('users/update/contacts/', data);
  }

  getInterests(): Observable<Interest> {
    return this.cHttp.get('users/update/interests/');
  }

  saveInterests(data: any): Observable<any> {
    return this.cHttp.put('users/update/interests/', data);
  }

  getSecondaryEducation(): Observable<Array<SecondaryEducation>> {
    return this.cHttp.get('users/update/educations/secondary/');
  }

  saveSecondaryEducation(data: any): Observable<any> {
    return this.cHttp.put('users/update/educations/secondary/', data);
  }

  deleteSecondaryEducation(uuid: string): Observable<any> {
    return this.cHttp.delete('users/update/educations/secondary/', {uuid});
  }

  getHigherEducation(): Observable<Array<HigherEducation>> {
    return this.cHttp.get('users/update/educations/higher/');
  }

  saveHigherEducation(data: any): Observable<any> {
    return this.cHttp.put('users/update/educations/higher/', data);
  }

  deleteHigherEducation(uuid: string): Observable<any> {
    return this.cHttp.delete('users/update/educations/higher/', {uuid});
  }

  getCareer(): Observable<Array<Career>> {
    return this.cHttp.get('users/update/jobs');
  }

  saveCareer(data: any): Observable<any> {
    return this.cHttp.put('users/update/jobs/', data);
  }

  deleteCareer(uuid: string): Observable<any> {
    return this.cHttp.delete('users/update/jobs/', {uuid});
  }

  getMilitaryService(): Observable<Array<MilitaryService>> {
    return this.cHttp.get('users/update/military-services/');
  }

  saveMilitaryService(data: any): Observable<any> {
    return this.cHttp.put('users/update/military-services/', data);
  }

  deleteMilitaryService(uuid: string): Observable<any> {
    return this.cHttp.delete('users/update/military-services/', {uuid});
  }

  getLifePosition(): Observable<LifePosition> {
    return this.cHttp.get('users/update/life-position/');
  }

  saveLifePosition(data: any): Observable<any> {
    return this.cHttp.put('users/update/life-position/', data);
  }

  getPortfolio(): Observable<Array<Portfolio>> {
    return this.cHttp.get('users/update/portfolios/');
  }

  savePortfolio(data: any): Observable<any> {
    return this.cHttp.put('users/update/portfolios/', data);
  }

  deletePortfolio(uuid: string): Observable<any> {
    return this.cHttp.delete('users/update/portfolios/', {uuid});
  }
}
