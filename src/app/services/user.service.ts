import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserProfile} from '../shared/interfaces/user-profile';
import {CustomHttpService} from './custom-http.service';
import {UserBaseInfo} from '../shared/interfaces/user-base-info';
import {FriendRequest} from '../shared/interfaces/friend-request';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userProfile: BehaviorSubject<UserBaseInfo> = new BehaviorSubject<UserBaseInfo>(null);

  constructor(
    private cHttp: CustomHttpService
  ) {
  }

  getUserData(): void {
    const getInfo = this.getBaseInfo()
      .subscribe((data) => {
        this.userProfile.next(data);
        getInfo.unsubscribe();
      });
  }

  emptyUserData(): void {
    this.userProfile.next(null);
  }

  // Queries to backend

  getOverallProfile(uuid: string): Observable<UserProfile> {
    return this.cHttp.get(`users/profile/${uuid}`);
  }

  getBaseInfo(): Observable<UserBaseInfo> {
    return this.cHttp.get('users/profile/base-info/');
  }

  saveAvatar(formData: FormData): Observable<any> {
    return this.cHttp.put('users/update/avatar/', formData);
  }

  saveBackground(formData: FormData): Observable<any> {
    return this.cHttp.put('users/update/background-image/', formData);
  }

  getSomeoneBaseInfo(uuid: string): Observable<UserBaseInfo> {
    return this.cHttp.get(`users/profile/base-info/${uuid}`);
  }

  addToFriend(uuid: string): Observable<any> {
    return this.cHttp.post(`users/friends/add/`, {uuid});
  }

  getFriendRequests(): Observable<Array<FriendRequest>> {
    return this.cHttp.get('users/friends/unread-requests/');
  }

  replyFriendRequest(from_user: string, accept: boolean): Observable<any> {
    return this.cHttp.post('users/friends/reply/', {from_user, accept});
  }

  getFriends(): Observable<Array<UserBaseInfo>> {
    return this.cHttp.get('users/friends/my/');
  }
}
