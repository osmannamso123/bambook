import {Injectable} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {CustomHttpService} from './custom-http.service';
import {Company} from '../shared/interfaces/company';
import {CompanyAbout} from '../shared/interfaces/company-about';
import {URL_VALIDATION_PATTERN} from '../values/values';
import {CompanySocials} from '../shared/interfaces/company-socials';
import {CompanyStatute} from '../shared/interfaces/company-statute';
import {CompanyFile} from '../shared/interfaces/company-file';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  companyUuid: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    private fb: FormBuilder,
    private cHttp: CustomHttpService
  ) {}

  setCompanyUuid(uuid: string): void {
    this.companyUuid.next(uuid);
  }

  getMainForm(company?: Company): FormGroup {
    const is = !!company;
    return this.fb.group({
      uuid: [is ? company.uuid : null],
      name: [is ? company.name : '', Validators.required],
      slogan: [is ? company.slogan : '']
    });
  }

  getAboutForm(about: CompanyAbout): FormGroup {
    return this.fb.group({
      website: [about.website, Validators.pattern(URL_VALIDATION_PATTERN)],
      phone: [about.phone],
      additional_phone: [about.additional_phone],
      fax: [about.fax],
      iban: [about.iban],
      type_of_business: [about.type_of_business],
      country: [about.country ? about.country.uuid : ''],
      city: [about.city ? about.city.uuid : ''],
      street: [about.street],
      post_address: [about.post_address],
      working_days: [about.working_days],
      working_time: [about.working_time]
    });
  }

  getSocialsForm(socials: CompanySocials): FormGroup {
    return this.fb.group({
      facebook: [socials.facebook, Validators.pattern(URL_VALIDATION_PATTERN)],
      whatsapp: [socials.whatsapp, Validators.pattern(URL_VALIDATION_PATTERN)],
      instagram: [socials.instagram, Validators.pattern(URL_VALIDATION_PATTERN)],
      linkedin: [socials.linkedin, Validators.pattern(URL_VALIDATION_PATTERN)],
      piscart: [socials.piscart, Validators.pattern(URL_VALIDATION_PATTERN)],
      skype: [socials.skype, Validators.pattern(URL_VALIDATION_PATTERN)],
      viber: [socials.viber, Validators.pattern(URL_VALIDATION_PATTERN)],
      telegram: [socials.telegram, Validators.pattern(URL_VALIDATION_PATTERN)],
      wechat: [socials.wechat, Validators.pattern(URL_VALIDATION_PATTERN)],
      vk: [socials.vk, Validators.pattern(URL_VALIDATION_PATTERN)],
      google: [socials.google, Validators.pattern(URL_VALIDATION_PATTERN)],
      youtube: [socials.youtube, Validators.pattern(URL_VALIDATION_PATTERN)]
    });
  }

  getStatuteForm(statute: CompanyStatute): FormGroup {
    return this.fb.group({
      first: [statute.first],
      second: [statute.second],
      third: [statute.third],
      fourth: [statute.fourth],
      fifth: [statute.fifth],
      sixth: [statute.sixth]
    });
  }

  getClientWorkersForm(): FormArray {
    return this.fb.array([this.fb.group({
      name: ['', Validators.required],
      document: ['', Validators.required]
    }, {updateOn: 'blur'})]);
  }

  addClientWorkerForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      document: ['', Validators.required]
    });
  }

  getSuppliersForm(): FormArray {
    return this.fb.array([this.fb.group({
      name: ['', Validators.required],
      document: ['', Validators.required],
      contacts: ['', Validators.required]
    }, {updateOn: 'blur'})]);
  }

  addSuppliersForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      document: ['', Validators.required],
      contacts: ['', Validators.required]
    }, {updateOn: 'blur'});
  }

  //  Http queries

  createBusiness(formData: FormData): Observable<any> {
    return this.cHttp.post('companies/create/', formData);
  }

  getCompany(uuid: string): Observable<Company> {
    return this.cHttp.get(`companies/base-info/${uuid}/`);
  }

  updateCompanyBaseInfo(uuid: string, formData: FormData): Observable<any> {
    return this.cHttp.put(`companies/base-info/${uuid}/`, formData);
  }

  getCompanyAbout(uuid: string): Observable<CompanyAbout> {
    return this.cHttp.get(`companies/about/${uuid}/`);
  }

  updateCompanyAbout(uuid: string, formGroup: FormGroup): Observable<any> {
    return this.cHttp.put(`companies/about/${uuid}/`, formGroup.getRawValue());
  }

  getCompanySocials(uuid: string): Observable<CompanySocials> {
    return this.cHttp.get(`companies/social-networks/${uuid}/`);
  }

  updateCompanySocials(uuid: string, formGroup: FormGroup): Observable<any> {
    return this.cHttp.put(`companies/social-networks/${uuid}/`, formGroup.getRawValue());
  }

  getCompanyStatute(uuid: string): Observable<CompanyStatute> {
    return this.cHttp.get(`companies/statutes/${uuid}/`);
  }

  updateCompanyStatute(uuid: string, formGroup: FormGroup): Observable<any> {
    return this.cHttp.put(`companies/statutes/${uuid}/`, formGroup.getRawValue());
  }

  getCompanyPermissions(uuid: string): Observable<Array<CompanyFile>> {
    return this.cHttp.get(`companies/permission-files/${uuid}/`);
  }

  updateCompanyPermissions(uuid: string, formData: FormData): Observable<Array<CompanyFile>> {
    return this.cHttp.post(`companies/permission-files/${uuid}/`, formData);
  }

  getCompanyPhotos(uuid: string): Observable<Array<CompanyFile>> {
    return this.cHttp.get(`companies/images/${uuid}/`);
  }

  updateCompanyPhotos(uuid: string, formData: FormData): Observable<Array<CompanyFile>> {
    return this.cHttp.post(`companies/images/${uuid}/`, formData);
  }

  getCompanyVideos(uuid: string): Observable<Array<CompanyFile>> {
    return this.cHttp.get(`companies/videos/${uuid}/`);
  }

  updateCompanyVideos(uuid: string, formData: FormData): Observable<Array<CompanyFile>> {
    return this.cHttp.post(`companies/videos/${uuid}/`, formData);
  }

  getCompanyDocuments(uuid: string): Observable<Array<CompanyFile>> {
    return this.cHttp.get(`companies/documents/${uuid}/`);
  }

  updateCompanyDocuments(uuid: string, formData: FormData): Observable<Array<CompanyFile>> {
    return this.cHttp.post(`companies/documents/${uuid}/`, formData);
  }

  deleteCompanyPermission(uuid: string, company: string): Observable<any> {
    return this.cHttp.delete(`companies/permission-files/${company}/`, {uuid});
  }

  deleteCompanyPhoto(uuid: string, company: string): Observable<any> {
    return this.cHttp.delete(`companies/images/${company}/`, {uuid});
  }

  deleteCompanyVideo(uuid: string, company: string): Observable<any> {
    return this.cHttp.delete(`companies/videos/${company}/`, {uuid});
  }

  deleteCompanyDocument(uuid: string, company: string): Observable<any> {
    return this.cHttp.delete(`companies/documents/${company}/`, {uuid});
  }

  getCompanyClients(uuid: string): Observable<any> {
    return this.cHttp.get(`companies/clients/${uuid}/`);
  }

  addCompanyClients(uuid: string, formData): Observable<any> {
    return this.cHttp.post(`companies/clients/${uuid}/`, formData);
  }

  deleteCompanyClient(uuid: string, company: string): Observable<any> {
    return this.cHttp.delete(`companies/clients/${company}/`, {uuid});
  }

  getCompanyWorkers(uuid: string): Observable<any> {
    return this.cHttp.get(`companies/workers/${uuid}/`);
  }

  addCompanyWorkers(uuid: string, formData): Observable<any> {
    return this.cHttp.post(`companies/workers/${uuid}/`, formData);
  }

  deleteCompanyWorker(uuid: string, company: string): Observable<any> {
    return this.cHttp.delete(`companies/workers/${company}/`, {uuid});
  }

  getCompanySuppliers(uuid: string): Observable<any> {
    return this.cHttp.get(`companies/suppliers/${uuid}/`);
  }

  addCompanySuppliers(uuid: string, formData): Observable<any> {
    return this.cHttp.post(`companies/suppliers/${uuid}/`, formData);
  }

  deleteCompanySupplier(uuid: string, company: string): Observable<any> {
    return this.cHttp.delete(`companies/suppliers/${company}/`, {uuid});
  }
}
