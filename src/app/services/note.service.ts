import {Injectable} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {CustomHttpService} from './custom-http.service';
import {PAGINATION_LIMIT} from '../values/values';
import {NoteFilter} from '../shared/interfaces/note-filter';
import {Note} from '../shared/interfaces/note';
import {FormMethods} from '../shared/classes/form-methods';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  constructor(
    private fb: FormBuilder,
    private cHttp: CustomHttpService
  ) {
  }

  getNoteForm(note?: Note): FormGroup {
    const is = !!note;
    if (note?.deadline) {
      note.deadline = FormMethods.formatDashDate(note.deadline);
    }
    return this.fb.group({
      uuid: [is ? note.uuid : null],
      name: [is ? note.name : '', Validators.required],
      from_user: ['me', Validators.required],
      priority: [is ? note.priority : 0, Validators.required],
      chat: [is ? note.chat : false],
      tag: [is ? note.tag : ''],
      deadline: [is ? note.deadline : ''],
      text: [is ? note.text : '']
    });
  }

  getNoteMessageForm(uuid: string): FormGroup {
    return this.fb.group({
      author: uuid,
      message: '',
      command: 'new_message'
    });
  }

  getNotesFilterForm(): FormGroup {
    return this.fb.group({
      filtering: ['all'],
      sorting: ['recent'],
      from_date: [''],
      to_date: ['']
    });
  }

  // Http queries

  createNote(formData: FormData): Observable<any> {
    return this.cHttp.post('notes/create/', formData);
  }

  changeNote(uuid: string, formData: FormData): Observable<any> {
    return this.cHttp.put(`notes/${uuid}/`, formData);
  }

  getNotes(offset: number, filtering: NoteFilter, limit = PAGINATION_LIMIT): Observable<any> {
    return this.cHttp.get('notes/', {limit, offset, ...filtering});
  }

  replyNote(accept: boolean, uuid: string): Observable<any> {
    return this.cHttp.post('notes/reply/', {accept, uuid});
  }

  getNote(uuid: string): Observable<Note> {
    return this.cHttp.get(`notes/${uuid}/`);
  }
}
