import {Injectable} from '@angular/core';
import {CustomHttpService} from './custom-http.service';
import {Observable} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LocalStorageService} from './local-storage.service';
import {TOKEN_KEY} from '../values/local-storage-keys';
import {Router} from '@angular/router';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private cHttp: CustomHttpService,
    private fb: FormBuilder,
    private localStorage: LocalStorageService,
    private router: Router,
    private userService: UserService
  ) { }

  getLoginForm(): FormGroup {
    return this.fb.group({
      phone: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  getRegistrationForm(): FormGroup {
    return this.fb.group({
      phone: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      repeat_password: ['', Validators.required],
      terms: [false, Validators.pattern('true')]
    });
  }

  isAuthenticated(): boolean {
    return !!this.localStorage.getItem(TOKEN_KEY);
  }

  logOut(): void {
    this.localStorage.removeItem(TOKEN_KEY);
    this.router.navigateByUrl('/').then(() => {
      this.userService.emptyUserData();
    });
  }

  // Requests

  login(formGroup: FormGroup): Observable<any> {
    return this.cHttp.post('users/token/', formGroup.getRawValue());
  }

  sendCode(formGroup: FormGroup): Observable<any> {
    return this.cHttp.post('users/create/', formGroup.getRawValue());
  }

  activate(formGroup: FormGroup): Observable<any> {
    return this.cHttp.post('users/activate/', formGroup.getRawValue());
  }
}
