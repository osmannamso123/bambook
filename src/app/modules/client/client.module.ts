import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandingComponent} from '../../components/client/landing/landing.component';
import {HeaderComponent} from '../../components/client/header/header.component';
import {RouterModule} from '@angular/router';
import {LoginComponent} from '../../components/client/login/login.component';
import {RegistrationComponent} from '../../components/client/registration/registration.component';
import {AuthComponent} from '../../components/client/auth/auth.component';
import {NgxMaskModule} from 'ngx-mask';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserLandingComponent} from '../../components/user/user-landing/user-landing.component';
import {UIKitModule} from '../ui-kit.module';

@NgModule({
  declarations: [
    LandingComponent,
    HeaderComponent,
    AuthComponent,
    LoginComponent,
    RegistrationComponent,
    UserLandingComponent
  ],
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule,
    UIKitModule
  ]
})
export class ClientModule { }
