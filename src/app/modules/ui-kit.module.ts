import {NgModule} from '@angular/core';
import {SelectComponent} from '../components/ui-kit/select/select.component';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {DatepickerComponent} from '../components/ui-kit/datepicker/datepicker.component';
import {NgxMaskModule} from 'ngx-mask';
import {SnackbarComponent} from '../components/ui-kit/snackbar/snackbar.component';
import {UploadPhotoModal} from '../components/ui-kit/modals/upload-photo/upload-photo-modal.component';
import {WarningMessageComponent} from '../components/ui-kit/modals/warning-message/warning-message.component';
import {HeaderRightComponent} from '../components/ui-kit/header-right/header-right.component';
import {DragDropDirective} from '../shared/directives/drag-and-drop.directive';
import {BareSelectComponent} from '../components/ui-kit/bare-select/bare-select.component';
import {FileSizePipe} from '../shared/pipes/file-size.pipe';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    SelectComponent,
    DatepickerComponent,
    SnackbarComponent,
    UploadPhotoModal,
    WarningMessageComponent,
    HeaderRightComponent,
    DragDropDirective,
    BareSelectComponent,
    FileSizePipe
  ],
  exports: [
    SelectComponent,
    DatepickerComponent,
    SnackbarComponent,
    HeaderRightComponent,
    BareSelectComponent,
    DragDropDirective,
    FileSizePipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxMaskModule,
    RouterModule
  ]
})
export class UIKitModule {}
