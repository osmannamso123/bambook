import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {UserBaseInfo} from '../../shared/interfaces/user-base-info';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {NO_AVATAR_SMALL} from '../../values/values';
import {UserService} from '../../services/user.service';
import {FormControl} from '@angular/forms';
import {SearchService} from '../../services/search.service';
import {UserProfile} from '../../shared/interfaces/user-profile';
import {DestroyComponent} from '../../shared/components/destroy.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent extends DestroyComponent implements OnInit {
  showSearchDropdown = false;
  noAvatarSmall = NO_AVATAR_SMALL;
  userProfile: UserBaseInfo;
  userSearchControl: FormControl = new FormControl('');
  foundUsers: Array<UserProfile> = [];
  @ViewChild('searchDropdown') searchDropdown: ElementRef;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private searchService: SearchService
  ) {
    super();
  }

  ngOnInit(): void {
    this.userService.userProfile
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.userProfile = data;
      });
    this.userService.getUserData();
    this.searchValueChanges();
  }

  @HostListener('document:click', ['$event'])
  onGlobalClick(event): void {
    this.showSearchDropdown = this.searchDropdown.nativeElement.contains(event.target);
  }

  searchValueChanges(): void {
    this.userSearchControl.valueChanges
      .pipe(takeUntil(this.destroyed$), debounceTime(150))
      .subscribe((value: string) => {
        this.searchService.searchUsers(value)
          .pipe(takeUntil(this.destroyed$), debounceTime(150))
          .subscribe((data) => {
            this.foundUsers = data;
          });
      });
  }

  resetSearchControl(): void {
    this.userSearchControl.setValue('', {emitEvent: false});
  }
}
