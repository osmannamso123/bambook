import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {SettingsComponent} from '../../components/user/settings/settings.component';
import {ProfileComponent} from '../../components/user/profile/profile.component';
import {NotesComponent} from '../../components/user/notes/notes.component';
import {BusinessComponent} from '../../components/user/business/business.component';
import {SupportComponent} from '../../components/user/support/support.component';
import {EditProfileComponent} from '../../components/user/edit-profile/edit-profile.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MainComponent} from '../../components/user/create-business/main/main.component';
import {LayoutComponent} from '../../components/user/create-business/layout/layout.component';
import {AboutComponent} from '../../components/user/create-business/about/about.component';
import {SocialsComponent} from '../../components/user/create-business/socials/socials.component';
import {PermissionsComponent} from '../../components/user/create-business/permissions/permissions.component';
import {PhotosComponent} from '../../components/user/create-business/photos/photos.component';
import {VideosComponent} from '../../components/user/create-business/videos/videos.component';
import {ClientsComponent} from '../../components/user/create-business/clients/clients.component';
import {WorkersComponent} from '../../components/user/create-business/workers/workers.component';
import {SuppliersComponent} from '../../components/user/create-business/suppliers/suppliers.component';
import {DocumentsComponent} from '../../components/user/create-business/documents/documents.component';
import {StatuteComponent} from '../../components/user/create-business/statute/statute.component';
import {NgxMaskModule} from 'ngx-mask';
import {UIKitModule} from '../ui-kit.module';
import {ArchiveComponent} from '../../components/user/archive/archive.component';
import {CreateNoteComponent} from '../../components/user/create-note/create-note.component';
import { WatchNoteComponent } from '../../components/user/watch-note/watch-note.component';


@NgModule({
  declarations: [
    SettingsComponent,
    ProfileComponent,
    NotesComponent,
    BusinessComponent,
    SupportComponent,
    EditProfileComponent,
    MainComponent,
    LayoutComponent,
    AboutComponent,
    SocialsComponent,
    PermissionsComponent,
    PhotosComponent,
    VideosComponent,
    ClientsComponent,
    WorkersComponent,
    SuppliersComponent,
    DocumentsComponent,
    StatuteComponent,
    ArchiveComponent,
    CreateNoteComponent,
    WatchNoteComponent
  ],
    imports: [
        CommonModule,
        UserRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule,
        UIKitModule
    ]
})
export class UserModule { }
