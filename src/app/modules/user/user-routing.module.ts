import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ProfileComponent} from '../../components/user/profile/profile.component';
import {SettingsComponent} from '../../components/user/settings/settings.component';
import {NotesComponent} from '../../components/user/notes/notes.component';
import {BusinessComponent} from '../../components/user/business/business.component';
import {SupportComponent} from '../../components/user/support/support.component';
import {EditProfileComponent} from '../../components/user/edit-profile/edit-profile.component';
import {LayoutComponent} from '../../components/user/create-business/layout/layout.component';
import {MainComponent} from '../../components/user/create-business/main/main.component';
import {AboutComponent} from '../../components/user/create-business/about/about.component';
import {SocialsComponent} from '../../components/user/create-business/socials/socials.component';
import {PermissionsComponent} from '../../components/user/create-business/permissions/permissions.component';
import {PhotosComponent} from '../../components/user/create-business/photos/photos.component';
import {VideosComponent} from '../../components/user/create-business/videos/videos.component';
import {ClientsComponent} from '../../components/user/create-business/clients/clients.component';
import {SuppliersComponent} from '../../components/user/create-business/suppliers/suppliers.component';
import {DocumentsComponent} from '../../components/user/create-business/documents/documents.component';
import {StatuteComponent} from '../../components/user/create-business/statute/statute.component';
import {WorkersComponent} from '../../components/user/create-business/workers/workers.component';
import {ArchiveComponent} from '../../components/user/archive/archive.component';

const routes: Routes = [
  {path: 'settings', component: SettingsComponent},
  {path: 'notes', component: NotesComponent},
  {path: 'support', component: SupportComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'profile/edit', component: EditProfileComponent},
  {path: 'profile/:uuid', component: ProfileComponent},
  {path: 'archives', component: ArchiveComponent},
  {path: 'business/create', component: LayoutComponent,
    children: [
      {path: '', component: MainComponent}
    ]},
  {path: 'business/:uuid', component: BusinessComponent},
  {path: 'business/:uuid/change', component: LayoutComponent,
   children: [
     {path: 'main', component: MainComponent},
     {path: 'about', component: AboutComponent},
     {path: 'socials', component: SocialsComponent},
     {path: 'statute', component: StatuteComponent},
     {path: 'permissions', component: PermissionsComponent},
     {path: 'photos', component: PhotosComponent},
     {path: 'videos', component: VideosComponent},
     {path: 'clients', component: ClientsComponent},
     {path: 'workers', component: WorkersComponent},
     {path: 'suppliers', component: SuppliersComponent},
     {path: 'documents', component: DocumentsComponent}
   ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
