import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ClientComponent} from './modules/client/client.component';
import {LandingComponent} from './components/client/landing/landing.component';
import {LoginComponent} from './components/client/login/login.component';
import {RegistrationComponent} from './components/client/registration/registration.component';
import {AuthComponent} from './components/client/auth/auth.component';
import {UserComponent} from './modules/user/user.component';
import {AuthGuard} from './shared/guards/auth-guard';
import {UserLandingComponent} from './components/user/user-landing/user-landing.component';

const routes: Routes = [
  {
    path: '', component: ClientComponent,
    children: [
      {path: '', component: LandingComponent},
      {path: '', component: AuthComponent,
        children: [
          {path: 'registration', component: RegistrationComponent},
          {path: 'login', component: LoginComponent}
        ]},
    ],
  },
  {path: 'landing/user/:uuid', component: UserLandingComponent},
  {path: 'user', component: UserComponent, loadChildren: './modules/user/user.module#UserModule', canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
