import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BareSelectComponent} from './bare-select.component';

describe('BareSelectComponent', () => {
  let component: BareSelectComponent;
  let fixture: ComponentFixture<BareSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BareSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BareSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
