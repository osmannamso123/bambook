import {Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {KeyValue} from '../../../shared/interfaces/key-value';
import {FormControl} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-bare-select',
  templateUrl: './bare-select.component.html',
  styleUrls: ['./bare-select.component.scss']
})
export class BareSelectComponent extends DestroyComponent implements OnInit {
  _options: Array<KeyValue> = [];
  @Input() control: FormControl;
  @Input() set options(value: Array<KeyValue>) {
    this.changeValue.next(value);
    this._options = value;
  }
  get options(): Array<KeyValue> {
    return this._options;
  }
  showDropdown = false;
  currentName: string;
  changeValue: BehaviorSubject<Array<KeyValue>> = new BehaviorSubject<Array<KeyValue>>([]);
  @ViewChild('itemList') itemList: ElementRef;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.changeValue
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        const foundOption = data.find((v) => v.uuid == this.control.value);
        if (foundOption) {
          this.currentName = foundOption.name;
        }
      });
  }

  @HostListener('document:click', ['$event'])
  onGlobalClick(event): void {
    if (this.showDropdown && !this.itemList.nativeElement.contains(event.target)) {
      this.showDropdown = false;
    }
  }

  dropdownShow(): void {
    this.showDropdown = !this.showDropdown;
  }

  select(keyValue: KeyValue): void {
    this.currentName = keyValue.name;
    this.control.setValue(keyValue.uuid);
    this.showDropdown = false;
  }
}
