import {Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {Day} from '../../../shared/models/day';
import * as moment from 'moment';
import {FormControl} from '@angular/forms';
import {DATEPICKER_STEPS, DATEPICKER_TOGGLE_RANGE, YEAR_MONTHS} from '../../../values/values';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent implements OnInit {
  @Input() control: FormControl;
  @Input() placeholder = 'dd/mm/YYYY';
  @Input() maxDate: Day;
  @Input() minDate: Day;
  days: Array<Day>;
  extraDays: number;
  month: number;
  year: number;
  rangeWord: string;
  showCalendar = false;
  currentStep = DATEPICKER_STEPS.DAY;
  datePickerSteps = DATEPICKER_STEPS;
  years: Array<number>;
  lastShowYear: number;
  months = YEAR_MONTHS;
  @ViewChild('input') inputEl: ElementRef;
  @ViewChild('calendar') calendarEl: ElementRef;

  constructor(
    private eRef: ElementRef
  ) { }

  ngOnInit(): void {
    this.reset();
    this.generate();
  }

  select(index: number): void {
    this.control.setValue(`${this.addZero(this.days[index].date)}${this.addZero(this.days[index].month + 1)}${this.days[index].year}`);
    this.showCalendar = false;
  }

  addZero(num: number): string {
    return num.toString().length == 1 ? `0${num}` : num.toString();
  }

  generate(): void {
    const currentDate = moment({date: 1, month: this.month, year: this.year});
    this.rangeWord = `${moment({month: this.month, year: this.year}).format('MMM YYYY')}`;
    let daysBefore = 0;
    this.days = [];
    while (daysBefore < currentDate.day()) {
      this.days.push(null);
      daysBefore += 1;
    }
    this.extraDays = daysBefore;
    for (let i = 1; i <= currentDate.daysInMonth(); i++) {
      const day = new Day(i,this.month, this.year);
      if (this.control.value?.length == 8) {
        const dateParams = this.getDateParams(this.control.value);
        day.selected = day.date == dateParams.date && day.month == dateParams.month && day.year == dateParams.year;
      }
      if (this.maxDate) {
        day.disabled = day.isMore(this.maxDate);
      }
      if (!day.disabled && this.minDate) {
        day.disabled = this.minDate.isMore(day);
      }
      this.days.push(day);
    }
  }

  setYearPickers(): void {
    if (this.currentStep == DATEPICKER_STEPS.DAY) {
      this.currentStep = DATEPICKER_STEPS.YEAR;
      this.lastShowYear = this.year || new Date().getFullYear();
      this.generateYears();
    } else if (this.currentStep == DATEPICKER_STEPS.YEAR) {
      this.currentStep = DATEPICKER_STEPS.DAY;
      this.rangeWord = `${moment({month: this.month, year: this.year}).format('MMM YYYY')}`;
    }
  }

  generateYears(): void {
    this.years = [];
    for (let i = this.lastShowYear - 20; i <= this.lastShowYear; i++) {
      this.years.push(i);
    }
    this.rangeWord = `${this.lastShowYear - 20} - ${this.lastShowYear}`;
  }

  @HostListener('document:click', ['$event'])
  clickOut(event): void {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.showCalendar = false;
    }
  }

  next(): void {
    if (this.currentStep == DATEPICKER_STEPS.DAY) {
      this.month += 1;
      if (this.month > 11) {
        this.month = 0;
        this.year += 1;
      }
      this.generate();
    } else if (this.currentStep == DATEPICKER_STEPS.YEAR) {
      this.lastShowYear += 21;
      this.generateYears();
    } else {
      this.year += 1;
      this.rangeWord = this.year.toString();
    }
  }

  prev(): void {
    if (this.currentStep == DATEPICKER_STEPS.DAY) {
      this.month -= 1;
      if (this.month < 0) {
        this.month = 11;
        this.year -= 1;
      }
      this.generate();
    } else if (this.currentStep == DATEPICKER_STEPS.YEAR) {
      this.lastShowYear -= 21;
      this.generateYears();
    } else {
      this.year -= 1;
      this.rangeWord = this.year.toString();
    }
  }

  clear(): void {
    this.control.setValue('');
    this.days.forEach((d) => {
      if (d) {
        d.selected = false;
      }
    });
  }

  displayCalendar(): void {
    if (this.showCalendar) {
      this.showCalendar = false;
    } else {
      if (window.screen.height - this.inputEl.nativeElement.getBoundingClientRect().bottom < DATEPICKER_TOGGLE_RANGE) {
        this.calendarEl.nativeElement.classList.add('on-top');
      }
      this.showCalendar = true;
      this.reset();
      this.generate();
    }
  }

  selectYear(event: MouseEvent, year: number): void {
    event.stopPropagation();
    this.year = year;
    this.currentStep = DATEPICKER_STEPS.MONTH;
    this.rangeWord = year.toString();
  }

  selectMonth(event: MouseEvent, index: number): void {
    event.stopPropagation();
    this.month = index;
    this.currentStep = DATEPICKER_STEPS.DAY;
    this.rangeWord = `${YEAR_MONTHS[index]} ${this.year}`;
    this.generate();
  }

  reset(): void {
    const defaultReset = () => {
      const nowDate = moment();
      this.month = nowDate.month();
      this.year = nowDate.year();
    }
    if (this.control.value?.length == 8) {
      const dateParams = this.getDateParams(this.control.value);
      const checkDate = moment(dateParams);
      if (checkDate.isValid()) {
        this.month = dateParams.month;
        this.year = dateParams.year;
      } else {
        defaultReset();
      }
    } else {
      defaultReset();
    }
  }

  getDateParams(dateFormat: string): Day {
    const month = +dateFormat.substr(2, 2) - 1;
    const year = +dateFormat.substr(4, 4);
    const date = +dateFormat.substr(0, 2);

    return new Day( date, month, year);
  }
}
