import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {UiWindowService} from '../../../services/ui-window.service';
import {SnackbarData} from '../../../shared/interfaces/snackbar-data';
import {ReplaySubject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent extends DestroyComponent implements AfterViewInit {
  snackBarData: SnackbarData;
  @ViewChild('snackBarCover') snackBarCover: ElementRef;

  constructor(
    private uiWindow: UiWindowService
  ) {
    super();
  }

  ngAfterViewInit(): void {
    this.uiWindow.getSnackBar()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.snackBarData = data;
        this.snackBarCover.nativeElement.classList.add('with-data');
        this.snackBarCover.nativeElement.classList.add(data.purpose);
        setTimeout(() => {
          this.snackBarCover.nativeElement.classList.remove('with-data');
          this.snackBarCover.nativeElement.classList.remove(data.purpose);
        }, data.delay);
      });
  }

}
