import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {KeyValue} from '../../../shared/interfaces/key-value';
import {FormControl} from '@angular/forms';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent extends DestroyComponent implements OnInit {
  @Input() control: FormControl;
  @Input() options: Array<KeyValue>;
  @Input() isEditable = true;
  @Input() placeholder = '';
  @Output() searching: EventEmitter<string> = new EventEmitter<string>();
  searchControl: FormControl = new FormControl('');
  showOptions = false;

  constructor(
    private eRef: ElementRef
  ) {
    super();
  }

  ngOnInit(): void {
    if (this.isEditable) {
      this.searchValueChanges();
    }
    if (this.control.value != null) {
      this.options.forEach((keyValue) => {
        if (keyValue.uuid == this.control.value) {
          this.searchControl.setValue(keyValue.name, {emitEvent: false});
        }
      });
    }
  }

  @HostListener('document:click', ['$event'])
  clickOut(event): void {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.showOptions = false;
    }
  }

  searchValueChanges(): void {
    this.searchControl.valueChanges
      .pipe(takeUntil(this.destroyed$), debounceTime(150))
      .subscribe((value: string) => {
        this.showOptions = true;
        if (!value.length) {
          this.control.setValue('');
        }
        this.searching.emit(value);
      });
  }

  selectOption(obj: KeyValue): void {
    this.control.setValue(obj.uuid);
    this.searchControl.setValue(obj.name, {emitEvent: false});
    this.showOptions = false;
  }

  showOptionsArrow(): void {
    this.showOptions = !this.showOptions;
    this.searching.emit(this.searchControl.value);
  }
}
