import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-warning-message',
  templateUrl: './warning-message.component.html',
  styleUrls: ['./warning-message.component.scss']
})
export class WarningMessageComponent {
  message: string;

  constructor(
    private dialogRef: MatDialogRef<WarningMessageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {message: string}
  ) { }

  close(): void {
    this.dialogRef.close();
  }

}
