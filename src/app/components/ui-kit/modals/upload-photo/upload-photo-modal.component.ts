import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {IMAGE_EXTENSIONS} from '../../../../values/values';
import {ImageCheckComponent} from '../../../../shared/components/image-check.component';

@Component({
  selector: 'app-upload-photo-modal',
  templateUrl: './upload-photo-modal.component.html',
  styleUrls: ['./upload-photo-modal.component.scss']
})
export class UploadPhotoModal extends ImageCheckComponent implements OnInit {
  @Output() savePhoto: EventEmitter<File> = new EventEmitter<File>();
  picture: string | ArrayBuffer;
  file: File;
  step = 1;
  allowedFormats = IMAGE_EXTENSIONS;

  constructor(
    public dialogRef: MatDialogRef<UploadPhotoModal>,
    public dialog: MatDialog
  ) {
    super(dialog);
  }

  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close()
  }

  fileDropped(files: Array<File>): void {
    if (!this.checkForFormat(files)) {
      return;
    }
    const file = files[0];
    const reader = new FileReader();

    reader.onload = (e) => {
      this.picture = e.target.result;
      this.file = file;
      this.step = 2;
    }

    reader.readAsDataURL(file);
  }

  fileChanged(event: Event): void {
    const filesForCheck = Array.from((event.target as HTMLInputElement).files);
    if (!this.checkForFormat(filesForCheck)) {
      return;
    }
    const input: any = document.getElementById('upload-photo');
    if (input.files?.length) {
      const reader = new FileReader();

      reader.onload = (e) => {
        this.picture = e.target.result;
        this.file = (event.target as HTMLInputElement).files.item(0);
        this.step = 2;
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  back(): void {
    (document.getElementById('upload-photo') as HTMLInputElement).value = '';
    this.step = 1;
  }

  save(): void {
    this.savePhoto.emit(this.file);
  }
}
