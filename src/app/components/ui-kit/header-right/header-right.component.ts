import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {NO_AVATAR, NO_AVATAR_SMALL} from '../../../values/values';
import {UserService} from '../../../services/user.service';
import {AuthService} from '../../../services/auth.service';
import {UserBaseInfo} from '../../../shared/interfaces/user-base-info';
import {NotificationsService} from '../../../services/notifications.service';
import {Notification} from '../../../shared/interfaces/notification';
import {NoteService} from '../../../services/note.service';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-header-right',
  templateUrl: './header-right.component.html',
  styleUrls: ['./header-right.component.scss']
})
export class HeaderRightComponent extends DestroyComponent implements OnInit {
  showUserDropdown = false;
  showNotificationDropdown = false;
  userProfile: UserBaseInfo;
  notifications: Array<Notification>;
  noAvatar = NO_AVATAR_SMALL;
  notificationCount = 0;
  @ViewChild('userDropdown') userDropdown: ElementRef;
  @ViewChild('notificationDropdown') notificationDropdown: ElementRef;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private notificationsService: NotificationsService,
    private noteService: NoteService
  ) {
    super();
  }

  ngOnInit(): void {
    this.userService.userProfile
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.userProfile = data;
      });
    this.notificationsService.notificationCount
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.notificationCount = data;
      })
  }

  @HostListener('document:click', ['$event'])
  onGlobalClick(event): void {
    if (this.showUserDropdown && !this.userDropdown.nativeElement.contains(event.target)) {
      this.showUserDropdown = false;
    } else if (this.showNotificationDropdown && !this.notificationDropdown.nativeElement.contains(event.target)) {
      this.showNotificationDropdown = false;
    }
  }

  notificationDropdownShow(): void {
    if (this.showNotificationDropdown) {
      this.showNotificationDropdown = false;
    } else {
      this.getNotifications();
      this.notificationsService.readAll();
      this.showNotificationDropdown = true;
    }
  }

  closeNotificationDropdown(): void {
    this.showNotificationDropdown = false;
  }

  getNotifications(): void {
    this.notificationsService.getNotifications()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data: any) => {
        this.notifications = data.results;
      });
  }

  replyFriendRequest(uuid: string, accept: boolean, index: number, event: MouseEvent): void {
    event.stopPropagation();
    this.notifications.splice(index, 1);
    this.userService.replyFriendRequest(uuid, accept)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
  }

  replyNote(uuid: string, accept: boolean, index: number, event: MouseEvent): void {
    event.stopPropagation();
    this.notifications.splice(index, 1);
    this.noteService.replyNote(accept, uuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
  }

  showDropdown(): void {
    this.showUserDropdown = !this.showUserDropdown;
  }

  logOut(): void {
    this.authService.logOut();
  }
}
