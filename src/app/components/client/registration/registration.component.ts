import {Component,  OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {LoginResponse} from '../../../shared/interfaces/login-response';
import {TOKEN_KEY} from '../../../values/local-storage-keys';
import {LocalStorageService} from '../../../services/local-storage.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {UserService} from '../../../services/user.service';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent extends DestroyComponent implements OnInit {
  registrationForm: FormGroup;
  gotSMS = false;

  constructor(
    private authService: AuthService,
    private localStorage: LocalStorageService,
    private router: Router,
    private userService: UserService
  ) {
    super();
  }

  ngOnInit(): void {
    this.registrationForm = this.authService.getRegistrationForm();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  sendCode(): void {
    this.registrationForm.markAllAsTouched();
    if (this.registrationForm.valid) {
      if (this.registrationForm.get('password').value !== this.registrationForm.get('repeat_password').value) {
        this.registrationForm.get('repeat_password').setErrors({notMatch: true});
        return;
      }
      this.authService.sendCode(this.registrationForm)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.registrationForm.addControl('code', new FormControl('', Validators.required));
          this.gotSMS = true;
        }, (err: HttpErrorResponse) => {
          this.errorHandle(err);
        });
    }
  }

  activate(): void {
    this.registrationForm.markAllAsTouched();
    if (this.registrationForm.valid) {
      this.authService.activate(this.registrationForm)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data: LoginResponse) => {
          this.localStorage.setItem(TOKEN_KEY, data.access);
          this.userService.getUserData();
          this.router.navigateByUrl('/user/profile');
        }, (err: HttpErrorResponse) => {
          this.errorHandle(err);
        });
    }
  }

  errorHandle(err: HttpErrorResponse): void {
    if (err.status == 400) {
      Object.keys(err.error).forEach((key) => {
        err.error[key].forEach((value) => {
          this.registrationForm.get(key).setErrors({[value]: true});
        });
      });
    } else {
      this.registrationForm.setErrors({unknown: true});
    }
  }
}
