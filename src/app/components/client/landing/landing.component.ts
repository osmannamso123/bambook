import {Component, OnInit} from '@angular/core';
import {LandingDataService} from '../../../services/landing-data.service';
import {takeUntil} from 'rxjs/operators';
import {Reviewer} from '../../../shared/interfaces/reviewer';
import {Advantage} from '../../../shared/interfaces/advantage';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent extends DestroyComponent implements OnInit {
  reviewers: Array<Reviewer>;
  activeReviewer = 0;
  advantages: Array<Advantage>;

  constructor(
    private landingDataService: LandingDataService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getLandingData();
  }

  setActiveReviewer(index: number): void {
    this.activeReviewer = index;
  }

  getLandingData(): void {
    this.landingDataService.getReviewers()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.reviewers = data;
      });
    this.landingDataService.getAdvantages()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.advantages = data;
      });
  }
}
