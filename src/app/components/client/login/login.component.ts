import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {LoginResponse} from '../../../shared/interfaces/login-response';
import {LocalStorageService} from '../../../services/local-storage.service';
import {TOKEN_KEY} from '../../../values/local-storage-keys';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user.service';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent extends DestroyComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private localStorage: LocalStorageService,
    private router: Router,
    private userService: UserService
  ) {
    super();
  }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/user/profile']);
    }
    this.loginForm = this.authService.getLoginForm();
  }

  login(): void {
    this.authService.login(this.loginForm)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data: LoginResponse) => {
        this.localStorage.setItem(TOKEN_KEY, data.access);
        this.userService.getUserData();
        this.router.navigateByUrl('/user/profile');
      }, () => {
        this.loginForm.setErrors({invalid: true});
      });
  }
}
