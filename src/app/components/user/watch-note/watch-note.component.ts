import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NoteService} from '../../../services/note.service';
import {delay, retryWhen, take, takeUntil} from 'rxjs/operators';
import {Note} from '../../../shared/interfaces/note';
import {NO_AVATAR_SMALL, URGENCY_NAMES} from '../../../values/values';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {environment} from '../../../../environments/environment';
import {UserBaseInfo} from '../../../shared/interfaces/user-base-info';
import {UserService} from '../../../services/user.service';
import {DestroyComponent} from '../../../shared/components/destroy.component';
import {Message} from '../../../shared/interfaces/message';
import {FormGroup} from '@angular/forms';
import {ChatResponse} from '../../../shared/interfaces/chat-response';

@Component({
  selector: 'app-watch-note[noteUuid]',
  templateUrl: './watch-note.component.html',
  styleUrls: ['./watch-note.component.scss']
})
export class WatchNoteComponent extends DestroyComponent {
  @Input() set noteUuid(uuid: string) {
    this.getNote(uuid);
  }
  noteReady: boolean;
  note: Note;
  meAuthor: boolean;
  urgencyNames = URGENCY_NAMES;
  noAvatar = NO_AVATAR_SMALL;
  connection: WebSocketSubject<any>;
  userData: UserBaseInfo;
  messages: Array<Message> = [];
  messageForm: FormGroup;
  @Output() replied: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() closed: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() edited: EventEmitter<Note> = new EventEmitter<Note>();

  constructor(
    private noteService: NoteService,
    private userService: UserService
  ) {
    super();
  }

  close(): void {
    this.closed.emit();
  }

  edit():void {
    this.edited.emit(this.note);
  }

  getNote(uuid: string): void {
    this.noteReady = false;
    this.noteService.getNote(uuid)
      .pipe(take(1))
      .subscribe((data) => {
        if (data.tag) {
          data.tags = data.tag.split('  ');
        }
        this.noteReady = true;
        this.note = data;
        if (data.chat) {
          this.getUser();
        } else {
          this.userService.userProfile
            .pipe(takeUntil(this.destroyed$))
            .subscribe((data) => {
              this.meAuthor = this.note.from_user.uuid == data.uuid;
            });
        }
      });
  }

  getUser(): void {
    this.userService.userProfile
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.userData = data;
        this.messageForm = this.noteService.getNoteMessageForm(data.uuid);
        this.connect(this.note.chat_uuid.replace(/-/g, ''));
      });
  }

  connect(uuid: string): void {
    if (this.connection) {
      this.connection.unsubscribe();
    }
    this.connection = webSocket({
      url: `${environment.wsUrl}chat/${uuid}/`
    });
    this.connection
      .pipe(retryWhen((errors) => errors.pipe(delay(2000))))
      .subscribe((data: ChatResponse) => {
        if (data.type == 'send_message') {
          data.messages.forEach((m) => {
            m.is_me = m.author.uuid == this.userData.uuid;
          });
          this.messages = data.messages.reverse();
        } else if (data.command == 'new_message') {
          data.message.is_me = data.message.author.uuid == this.userData.uuid;
          this.messages.unshift(data.message);
        }
      });
  }

  sendMessage(): void {
    this.connection.next(this.messageForm.getRawValue());
    this.clearMessageForm();
  }

  clearMessageForm(): void {
    this.messageForm.get('message').setValue('');
  }

  replyNote(accept: boolean): void {
    if (!accept) {
      this.closed.emit();
    }
    this.replied.emit(accept);
    this.note.is_request = false;
  }
}
