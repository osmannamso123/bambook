import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchNoteComponent } from './watch-note.component';

describe('WatchNoteComponent', () => {
  let component: WatchNoteComponent;
  let fixture: ComponentFixture<WatchNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
