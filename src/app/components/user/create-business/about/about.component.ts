import {Component, OnInit} from '@angular/core';
import {DestroyComponent} from '../../../../shared/components/destroy.component';
import {BusinessService} from '../../../../services/business.service';
import {takeUntil} from 'rxjs/operators';
import {FormGroup} from '@angular/forms';
import {UiWindowService} from '../../../../services/ui-window.service';
import {Router} from '@angular/router';
import {FormMethods} from '../../../../shared/classes/form-methods';
import {SearchService} from '../../../../services/search.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent extends DestroyComponent implements OnInit {
  aboutForm: FormGroup;
  countries = [];
  cities = [];
  companyUuid: string;

  constructor(
    private businessService: BusinessService,
    private uiWindow: UiWindowService,
    private router: Router,
    private searchService: SearchService
  ) {
    super();
  }

  ngOnInit(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
          this.businessService.getCompanyAbout(uuid)
            .pipe(takeUntil(this.destroyed$))
            .subscribe((data) => {
              if (data.city) {
                this.cities = [data.city];
              }
              if (data.country) {
                this.countries = [data.country];
              }
              this.aboutForm = this.businessService.getAboutForm(data);
            });
      });
  }

  submitForm(): void {
    this.aboutForm.markAllAsTouched();
    if (this.aboutForm.valid) {
      if (this.aboutForm.get('website').value) {
        this.aboutForm.get('website').setValue(FormMethods.addHttp(this.aboutForm.get('website').value));
      }
      FormMethods.emptyForm(this.aboutForm);
      this.businessService.updateCompanyAbout(this.companyUuid, this.aboutForm)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.uiWindow.showSnackBar({
            delay: 2000,
            message: 'Business updated',
            purpose: 'confirm'
          });
          this.router.navigate(['/user', 'business', this.companyUuid, 'change', 'socials']);
        });
    }
  }

  searchCountry(word: string): void {
    this.searchService.searchCountries(word)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.countries = data;
      });
  }

  searchCity(word: string): void {
    this.searchService.searchCities(word, this.aboutForm.get('country').value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.cities = data;
      });
  }
}
