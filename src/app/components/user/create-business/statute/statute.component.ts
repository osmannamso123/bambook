import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BusinessService} from '../../../../services/business.service';
import {UiWindowService} from '../../../../services/ui-window.service';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../../shared/components/destroy.component';
import {FormMethods} from '../../../../shared/classes/form-methods';

@Component({
  selector: 'app-statute',
  templateUrl: './statute.component.html',
  styleUrls: ['./statute.component.scss']
})
export class StatuteComponent extends DestroyComponent implements OnInit {
  companyUuid;
  statuteForm: FormGroup;

  constructor(
    private businessService: BusinessService,
    private uiWindow: UiWindowService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanyStatute(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.statuteForm = this.businessService.getStatuteForm(data);
          });
      });
  }

  submitForm(): void {
    this.statuteForm.markAllAsTouched();
    if (this.statuteForm.valid) {
      FormMethods.emptyForm(this.statuteForm);
      this.businessService.updateCompanyStatute(this.companyUuid, this.statuteForm)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.uiWindow.showSnackBar({
            delay: 2000,
            message: 'Business updated',
            purpose: 'confirm'
          });
          this.router.navigate(['/user', 'business', this.companyUuid, 'change', 'permissions']);
        });
    }
  }
}
