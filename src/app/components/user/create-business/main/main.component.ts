import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ImageCheckComponent} from '../../../../shared/components/image-check.component';
import {MatDialog} from '@angular/material/dialog';
import {IMAGE_EXTENSIONS} from '../../../../values/values';
import {FormGroup} from '@angular/forms';
import {BusinessService} from '../../../../services/business.service';
import {take, takeUntil} from 'rxjs/operators';
import {UserService} from '../../../../services/user.service';
import {Router} from '@angular/router';
import {UiWindowService} from '../../../../services/ui-window.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent extends ImageCheckComponent implements OnInit {
  mainForm: FormGroup;
  file: File;
  picture: string | ArrayBuffer;
  allowedFormats = IMAGE_EXTENSIONS;
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private businessService: BusinessService,
    private userService: UserService,
    private uiWindow: UiWindowService
  ) {
    super(dialog);
  }

  ngOnInit(): void {
    this.getCompany();
  }

  getCompany(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        if (uuid) {
          this.businessService.getCompany(uuid)
            .pipe(takeUntil(this.destroyed$))
            .subscribe((data) => {
              this.mainForm = this.businessService.getMainForm(data);
              if (data.logo) {
                this.picture = data.logo;
              }
            });
        } else {
          this.mainForm = this.businessService.getMainForm();
        }
      });
  }

  filesDropped(files: Array<File>): void {
    if (!this.checkForFormat(files)) {
      return;
    }
    const reader = new FileReader();
    this.file = files[0];
    reader.onload = (e) => {
      this.picture = e.target.result;
    }

    reader.readAsDataURL(this.file);
  }

  filesChanged(event: Event): void {
    const filesForCheck = Array.from((event.target as HTMLInputElement).files);
    if (!this.checkForFormat(filesForCheck)) {
      return;
    }
    const input = this.fileInput.nativeElement;
    if (input.files.length) {
      this.file = (event.target as HTMLInputElement).files.item(0);
      const reader = new FileReader();

      reader.onload = (e) => {
        this.picture = e.target.result;
        this.fileInput.nativeElement.value = '';
      }

      reader.readAsDataURL(this.file);
    }
  }

  submitForm(): void {
    this.mainForm.markAllAsTouched();
    if (this.mainForm.valid) {
      const formData = new FormData();
      if (this.file) {
        formData.append('logo', this.file, this.file.name);
      }
      formData.append('name', this.mainForm.get('name').value);
      formData.append('slogan', this.mainForm.get('slogan').value);
      if (this.mainForm.get('uuid').value) {
        this.businessService.updateCompanyBaseInfo(this.mainForm.get('uuid').value, formData)
          .pipe(take(1))
          .subscribe(() => {
            this.router.navigate(['/user', 'business', this.mainForm.get('uuid').value, 'change', 'about']);
            this.uiWindow.showSnackBar({
              delay: 2000,
              message: 'Business updated',
              purpose: 'confirm'
            });
            this.userService.getUserData();
          }, (err: HttpErrorResponse) => {
            this.errorHandle(err);
          });
      } else {
        this.businessService.createBusiness(formData)
          .pipe(take(1))
          .subscribe((data) => {
            this.router.navigate(['/user', 'business', data.uuid, 'change', 'about']);
            this.userService.getUserData();
          }, (err: HttpErrorResponse) => {
            this.errorHandle(err);
          });
      }
    }
  }

  errorHandle(err: HttpErrorResponse): void {
    if (err.status == 400) {
      Object.keys(err.error).forEach((key) => {
        err.error[key].forEach((value) => {
          this.mainForm.get(key).setErrors({[value]: true});
        });
      });
    }
  }
}
