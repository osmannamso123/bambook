import {Component, OnInit} from '@angular/core';
import {FormArray} from '@angular/forms';
import {BusinessService} from '../../../../services/business.service';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../../shared/components/destroy.component';
import {CompanyWorker} from '../../../../shared/interfaces/company-worker';
import {Router} from '@angular/router';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.scss']
})
export class WorkersComponent extends DestroyComponent implements OnInit {
  companyUuid: string;
  workers: Array<CompanyWorker> = [];
  workersForm: FormArray;

  constructor(
    private businessService: BusinessService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanyWorkers(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.workers = data;
            this.workersForm = this.businessService.getClientWorkersForm();
          });
      });
  }

  fileDropped(files: Array<File>, index: number): void {
    const file = files[0];
    this.workersForm.controls[index].get('document').setValue(file);
  }

  fileChanged(event: Event, index: number): void {
    this.workersForm.controls[index].get('document').setValue((event.target as HTMLInputElement).files.item(0));
  }

  addWorkerForm(): void {
    this.workersForm.push(this.businessService.addClientWorkerForm());
  }

  removeForm(index: number): void {
    this.workersForm.removeAt(index);
  }

  submitForm(): void {
    this.workersForm.markAllAsTouched();
    if (this.workersForm.valid) {
      const formData = new FormData();
      const workers = [];
      const documents = [];
      this.workersForm.controls.forEach((f) => {
        workers.push(f.get('name').value);
        documents.push(f.get('document').value);
      });
      documents.forEach((d) => {
        formData.append('documents', d, d.name);
      });
      formData.append('workers', JSON.stringify(workers));
      this.businessService.addCompanyWorkers(this.companyUuid, formData)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.router.navigate(['/user', 'business', this.companyUuid, 'change', 'suppliers']);
        });
    }
  }

  deleteWorker(index: number): void {
    this.businessService.deleteCompanyWorker(this.workers[index].uuid, this.companyUuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
    this.workers.splice(index, 1);
  }
}
