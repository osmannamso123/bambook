import {Component, OnInit} from '@angular/core';
import {FormArray} from '@angular/forms';
import {BusinessService} from '../../../../services/business.service';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../../shared/components/destroy.component';
import {CompanySupplier} from '../../../../shared/interfaces/company-supplier';
import {Router} from '@angular/router';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent extends DestroyComponent implements OnInit {
  companyUuid: string;
  suppliers: Array<CompanySupplier> = [];
  supplierForm: FormArray;

  constructor(
    private businessService: BusinessService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanySuppliers(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.suppliers = data;
            this.supplierForm = this.businessService.getSuppliersForm();
          });
      });
  }

  fileDropped(files: Array<File>, index: number): void {
    const file = files[0];
    this.supplierForm.controls[index].get('document').setValue(file);
  }

  fileChanged(event: Event, index: number): void {
    this.supplierForm.controls[index].get('document').setValue((event.target as HTMLInputElement).files.item(0));
  }

  addSupplierForm(): void {
    this.supplierForm.push(this.businessService.addSuppliersForm());
  }

  removeForm(index: number): void {
    this.supplierForm.removeAt(index);
  }

  submitForm(): void {
    this.supplierForm.markAllAsTouched();
    if (this.supplierForm.valid) {
      const formData = new FormData();
      const suppliers = [];
      const documents = [];
      const contacts = [];
      this.supplierForm.controls.forEach((f) => {
        suppliers.push(f.get('name').value);
        documents.push(f.get('document').value);
        contacts.push(f.get('contacts').value);
      });
      documents.forEach((d) => {
        formData.append('documents', d, d.name);
      });
      formData.append('suppliers', JSON.stringify(suppliers));
      formData.append('contacts', JSON.stringify(contacts));
      this.businessService.addCompanySuppliers(this.companyUuid, formData)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.router.navigate(['/user', 'business', this.companyUuid, 'change', 'documents']);
        });
    }
  }

  deleteSupplier(index: number): void {
    this.businessService.deleteCompanySupplier(this.suppliers[index].uuid, this.companyUuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
    this.suppliers.splice(index, 1);
  }
}
