import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {BusinessService} from '../../../../services/business.service';
import {finalize, takeUntil} from 'rxjs/operators';
import {ObjectMutates} from '../../../../shared/classes/object-mutates';
import {CompanyFilesComponent} from '../../../../shared/components/company-files.component';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent extends CompanyFilesComponent {
  constructor(
    public dialog: MatDialog,
    private businessService: BusinessService
  ) {
    super(dialog);
  }

  getFiles(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanyDocuments(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.files = ObjectMutates.addFileExtensions(data);
          });
      })
  }

  uploadFiles(files): void {
    const formData = new FormData();
    files.forEach((f) => {
      formData.append('files', f, f.name);
    });
    this.isAdding = true;
    this.businessService.updateCompanyDocuments(this.companyUuid, formData)
      .pipe(takeUntil(this.destroyed$), finalize(() => this.isAdding = false))
      .subscribe((data) => {
        this.files.push(...ObjectMutates.addFileExtensions(data));
        (document.getElementById('upload-documents') as HTMLInputElement).value = '';
      });
  }

  deleteFile(uuid: string): void {
    this.businessService.deleteCompanyDocument(uuid, this.companyUuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
  }
}
