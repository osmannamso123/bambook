import {Component, OnInit} from '@angular/core';
import {BusinessService} from '../../../../services/business.service';
import {FormArray} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {DestroyComponent} from '../../../../shared/components/destroy.component';
import {CompanyClient} from '../../../../shared/interfaces/company-client';
import {Router} from '@angular/router';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent extends DestroyComponent implements OnInit {
  companyUuid: string;
  clients: Array<CompanyClient> = [];
  clientsForm: FormArray;

  constructor(
    private businessService: BusinessService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanyClients(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.clients = data;
            this.clientsForm = this.businessService.getClientWorkersForm();
          });
      });
  }

  fileDropped(files: Array<File>, index: number): void {
    const file = files[0];
    this.clientsForm.controls[index].get('document').setValue(file);
  }

  fileChanged(event: Event, index: number): void {
    this.clientsForm.controls[index].get('document').setValue((event.target as HTMLInputElement).files.item(0));
  }

  addClientForm(): void {
    this.clientsForm.push(this.businessService.addClientWorkerForm());
  }

  removeForm(index: number): void {
    this.clientsForm.removeAt(index);
  }

  submitForm(): void {
    this.clientsForm.markAllAsTouched();
    if (this.clientsForm.valid) {
      const formData = new FormData();
      const clients = [];
      const documents = [];
      this.clientsForm.controls.forEach((f) => {
        clients.push(f.get('name').value);
        documents.push(f.get('document').value);
      });
      documents.forEach((d) => {
        formData.append('documents', d, d.name);
      });
      formData.append('clients', JSON.stringify(clients));
      this.businessService.addCompanyClients(this.companyUuid, formData)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.router.navigate(['/user', 'business', this.companyUuid, 'change', 'workers']);
        });
    }
  }

  deleteClient(index: number): void {
    this.businessService.deleteCompanyClient(this.clients[index].uuid, this.companyUuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
    this.clients.splice(index, 1);
  }
}
