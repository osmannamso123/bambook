import {Component} from '@angular/core';
import {IMAGE_EXTENSIONS} from '../../../../values/values';
import {CompanyFilesComponent} from '../../../../shared/components/company-files.component';
import {MatDialog} from '@angular/material/dialog';
import {BusinessService} from '../../../../services/business.service';
import {finalize, takeUntil} from 'rxjs/operators';
import {ObjectMutates} from '../../../../shared/classes/object-mutates';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent extends CompanyFilesComponent {
  constructor(
    public dialog: MatDialog,
    private businessService: BusinessService
  ) {
    super(dialog);
  }

  getFiles(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanyPhotos(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.files = ObjectMutates.addFileExtensions(data);
          });
      })
  }

  uploadFiles(files): void {
    const formData = new FormData();
    files.forEach((f) => {
      formData.append('files', f, f.name);
    });
    this.isAdding = true;
    this.businessService.updateCompanyPhotos(this.companyUuid, formData)
      .pipe(takeUntil(this.destroyed$), finalize(() => this.isAdding = false))
      .subscribe((data) => {
        this.files.push(...ObjectMutates.addFileExtensions(data));
        (document.getElementById('upload-photos') as HTMLInputElement).value = '';
      });
  }

  deleteFile(uuid: string): void {
    this.businessService.deleteCompanyPhoto(uuid, this.companyUuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
  }
}
