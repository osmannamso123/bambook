import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DestroyComponent} from '../../../../shared/components/destroy.component';
import {takeUntil} from 'rxjs/operators';
import {BusinessService} from '../../../../services/business.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent extends DestroyComponent implements OnInit {
  companyUuid: string;

  constructor(
    private route: ActivatedRoute,
    private businessService: BusinessService
  ) {
    super();
  }

  ngOnInit(): void {
    this.route.params
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        if (data.uuid) {
          this.companyUuid = data.uuid;
          this.businessService.setCompanyUuid(data.uuid);
        } else {
          this.businessService.setCompanyUuid(null);
        }
      });
  }

}
