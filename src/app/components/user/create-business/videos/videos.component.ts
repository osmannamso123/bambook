import {Component} from '@angular/core';
import {VIDEO_EXTENSIONS} from '../../../../values/values';
import {CompanyFilesComponent} from '../../../../shared/components/company-files.component';
import {MatDialog} from '@angular/material/dialog';
import {BusinessService} from '../../../../services/business.service';
import {finalize, takeUntil} from 'rxjs/operators';
import {ObjectMutates} from '../../../../shared/classes/object-mutates';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent extends CompanyFilesComponent {
  allowedFormats = VIDEO_EXTENSIONS;

  constructor(
    public dialog: MatDialog,
    private businessService: BusinessService
  ) {
    super(dialog);
  }

  getFiles(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanyVideos(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.files = ObjectMutates.addFileExtensions(data);
          });
      })
  }

  uploadFiles(files): void {
    const formData = new FormData();
    files.forEach((f) => {
      formData.append('files', f, f.name);
    });
    this.isAdding = true;
    this.businessService.updateCompanyVideos(this.companyUuid, formData)
      .pipe(takeUntil(this.destroyed$), finalize(() => this.isAdding = false))
      .subscribe((data) => {
        this.files.push(...ObjectMutates.addFileExtensions(data));
        (document.getElementById('upload-videos') as HTMLInputElement).value = '';
      });
  }

  deleteFile(uuid: string): void {
    this.businessService.deleteCompanyVideo(uuid, this.companyUuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        console.log(data);
      });
  }
}
