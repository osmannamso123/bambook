import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {BusinessService} from '../../../../services/business.service';
import {DestroyComponent} from '../../../../shared/components/destroy.component';
import {takeUntil} from 'rxjs/operators';
import {FormMethods} from '../../../../shared/classes/form-methods';
import {UiWindowService} from '../../../../services/ui-window.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-socials',
  templateUrl: './socials.component.html',
  styleUrls: ['./socials.component.scss']
})
export class SocialsComponent extends DestroyComponent implements OnInit {
  companyUuid;
  socialsForm: FormGroup;

  constructor(
    private businessService: BusinessService,
    private uiWindow: UiWindowService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.businessService.companyUuid
      .pipe(takeUntil(this.destroyed$))
      .subscribe((uuid) => {
        this.companyUuid = uuid;
        this.businessService.getCompanySocials(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            this.socialsForm = this.businessService.getSocialsForm(data);
          });
      });
  }


  submitForm(): void {
    this.socialsForm.markAllAsTouched();
    if (this.socialsForm.valid) {
      Object.keys(this.socialsForm.controls).forEach((key) => {
        if (this.socialsForm.controls[key].value) {
          this.socialsForm.controls[key].setValue(FormMethods.addHttp(this.socialsForm.controls[key].value));
        }
      });
      FormMethods.emptyForm(this.socialsForm);
      this.businessService.updateCompanySocials(this.companyUuid, this.socialsForm)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {
          this.uiWindow.showSnackBar({
            delay: 2000,
            message: 'Business updated',
            purpose: 'confirm'
          });
          this.router.navigate(['/user', 'business', this.companyUuid, 'change', 'statute']);
        });
    }
  }
}
