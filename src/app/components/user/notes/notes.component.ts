import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {NoteService} from '../../../services/note.service';
import {NO_AVATAR, NOTE_FILTER_OPTIONS, NOTE_SORT_OPTIONS, PAGINATION_LIMIT} from '../../../values/values';
import {take, takeUntil} from 'rxjs/operators';
import {NotePagination} from '../../../shared/interfaces/note-pagination';
import {Note} from '../../../shared/interfaces/note';
import {NoteFilter} from '../../../shared/interfaces/note-filter';
import {DestroyComponent} from '../../../shared/components/destroy.component';
import {Day} from '../../../shared/models/day';
import {FormMethods} from '../../../shared/classes/form-methods';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent extends DestroyComponent implements OnInit {
  noteForm: FormGroup;
  currentView: 'list' | 'box' = 'list';
  isAdding = false;
  isWatching = false;
  filterForm: FormGroup;
  filterOptions = NOTE_FILTER_OPTIONS;
  sortOptions = NOTE_SORT_OPTIONS;
  notePagination: NotePagination;
  noAvatar = NO_AVATAR;
  offset = 0;
  selectedNote: number;
  chosenNote: Note;
  dateFrom: Day;
  dateTo: Day;
  @ViewChild('watchElement') watchElement;
  trackByNotes = (note) => note.id;

  constructor(
    private noteService: NoteService
  ) {
    super();
  }

  ngOnInit(): void {
    this.filterForm = this.noteService.getNotesFilterForm();
    this.getNotes();
    this.filterChanges();
  }

  @HostListener('document:click', ['$event'])
  onGlobalClick(event): void {
    if (this.isWatching && !this.watchElement.nativeElement.contains(event.target)) {
      this.closeWatch();
    }
  }

  filterChanges(): void {
    this.filterForm.valueChanges
      .pipe(takeUntil(this.destroyed$))
      .subscribe((value: NoteFilter) => {
        this.offset = 0;
        if (value.to_date) {
          this.dateTo = FormMethods.formatSplitDate(value.to_date);
        }
        if (value.from_date) {
          this.dateFrom = FormMethods.formatSplitDate(value.from_date);
        }
        Object.keys(value).forEach((k) => {
          if (!value[k]) {
            delete value[k];
          }
        });
        this.getNotes(value, true);
      });
  }

  getNotes(noteFilter: NoteFilter = {sorting: 'recent'}, refresh = false): void {
    this.noteService.getNotes(this.offset, noteFilter)
      .pipe(take(1))
      .subscribe((data) => {
        if (this.notePagination && !refresh) {
          this.notePagination.results.push(...data.results);
          this.notePagination.next = data.next;
        } else {
          this.notePagination = data;
        }
      });
    this.offset += PAGINATION_LIMIT;
  }

  setView(view: 'list' | 'box'): void {
    this.currentView = view;
  }

  changeAdding(value: boolean): void {
    if (value) {
      this.noteForm = this.noteService.getNoteForm();
      this.closeWatch();
    } else {
      this.chosenNote = null;
    }
    this.isAdding = value;
  }

  replyNote(uuid: string, accept: boolean, index: number, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    this.noteService.replyNote(accept, uuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        if (data.success) {
          if (!accept) {
            this.notePagination.results.splice(index, 1);
          }
          this.notePagination.results[index].is_request = false;
        }
      });
  }

  createdNote(note: Note): void {
    this.isAdding = false;
    this.notePagination.results.unshift(note);
    this.offset += 1;
  }

  selectNote(event: MouseEvent, index: number): void {
    event.stopPropagation();
    this.isAdding = false;
    this.isWatching = true;
    this.selectedNote = index;
  }

  closeWatch(): void {
    this.isWatching = false;
  }

  startEditNote(note: Note): void {
    this.isWatching = false;
    this.noteForm = this.noteService.getNoteForm(note);
    this.chosenNote = note;
    this.isAdding = true;
  }

  edited(note: Note): void {
    this.isAdding = false;
    this.notePagination.results[this.selectedNote].name = note.name;
    this.notePagination.results[this.selectedNote].priority = note.priority;
    this.notePagination.results[this.selectedNote].text = note.text;
    this.notePagination.results[this.selectedNote].deadline = note.deadline;

  }
}
