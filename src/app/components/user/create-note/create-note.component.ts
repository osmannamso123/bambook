import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';
import {NoteService} from '../../../services/note.service';
import {FormControl, FormGroup} from '@angular/forms';
import {debounceTime, finalize, startWith, takeUntil} from 'rxjs/operators';
import {UserService} from '../../../services/user.service';
import {KeyValue} from '../../../shared/interfaces/key-value';
import {UserBaseInfo} from '../../../shared/interfaces/user-base-info';
import {NO_AVATAR} from '../../../values/values';
import {SearchService} from '../../../services/search.service';
import {Note} from '../../../shared/interfaces/note';
import {Day} from '../../../shared/models/day';
import {DestroyComponent} from '../../../shared/components/destroy.component';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.scss']
})
export class CreateNoteComponent extends DestroyComponent implements OnInit {
  @Input() note: Note;
  @Input() noteForm: FormGroup;
  nameEditing = false;
  senderOptions: Array<KeyValue> = [];
  showUserDropdown = false;
  showFileDropdown = false;
  userSearchControl: FormControl = new FormControl('');
  foundFriends: Array<UserBaseInfo> = [];
  chosenFriends: Array<UserBaseInfo> = [];
  noAvatar = NO_AVATAR;
  files: Array<File> = [];
  saving = false;
  today: Day;
  @Output() edited: EventEmitter<Note> = new EventEmitter<Note>();
  @Output() created: EventEmitter<Note> = new EventEmitter<Note>();
  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('userDropdown') userDropdown: ElementRef;
  @ViewChild('fileDropdown') fileDropdown: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(
    private noteService: NoteService,
    private userService: UserService,
    private searchService: SearchService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getSenderOptions();
    this.searchValues();
    const date = new Date();
    this.today = new Day(date.getDate(), date.getMonth(), date.getFullYear());
  }

  @HostListener('document:click', ['$event'])
  onGlobalClick(event): void {
    if (this.showUserDropdown && !this.userDropdown.nativeElement.contains(event.target)) {
      this.showUserDropdown = false;
    } else if (this.showFileDropdown && !this.fileDropdown.nativeElement.contains(event.target)) {
      this.showFileDropdown = false;
    }
  }

  searchValues(): void {
    this.userSearchControl.valueChanges
      .pipe(takeUntil(this.destroyed$), startWith(''))
      .subscribe((value: string) => {
        this.searchService.searchFriends(value)
          .pipe(takeUntil(this.destroyed$), debounceTime(150))
          .subscribe((data) => {
            this.foundFriends = data.filter((f) => this.chosenFriends.map((f2) => f2.uuid).indexOf(f.uuid) == -1);
          });
      });
  }

  getSenderOptions(): void {
    this.userService.userProfile
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.senderOptions = Object.assign([],[{
          uuid: 'me',
          name: data?.first_name + ' ' + data?.last_name
        }]);
      });
  }

  toggleNameEdit(): void {
    this.nameEditing = !this.nameEditing;
  }

  closeNote(): void {
    this.close.emit(true);
  }

  userDropdownShow(): void {
    this.showUserDropdown = !this.showUserDropdown;
  }

  fileDropdownShow(): void {
    this.showFileDropdown = !this.showFileDropdown;
  }

  filesDropped(files: Array<File>): void {
    this.files.push(...Array.from(files));
    this.fileInput.nativeElement.value = '';
  }

  filesChanged(event: Event): void {
    const input: any = this.fileInput.nativeElement;
    if (input.files.length) {
      this.files.push(...Array.from((event.target as HTMLInputElement).files));
      this.fileInput.nativeElement.value = '';
    }
  }

  removeFile(index: number, event: MouseEvent): void {
    event.stopPropagation();
    this.files.splice(index, 1);
  }

  chooseFriend(friend: UserBaseInfo, index: number, event: MouseEvent): void {
    event.stopPropagation();
    this.chosenFriends.push(friend);
    this.foundFriends.splice(index, 1);
  }

  removeFriend(index: number, event: MouseEvent): void {
    event.stopPropagation();
    if ((this.chosenFriends[index].first_name + ' ' + this.chosenFriends[index].last_name).includes(this.userSearchControl.value)) {
      this.foundFriends.push(this.chosenFriends[index]);
    }
    this.chosenFriends.splice(index, 1);
  }

  save(): void {
    this.noteForm.markAllAsTouched();
    if (this.noteForm.valid) {
      this.saving = true;
      const noteData = this.noteForm.getRawValue();
      noteData.participants = this.chosenFriends.map((f) => {
        return {uuid: f.uuid};
      });
      if (!noteData.deadline) {
        delete noteData.deadline;
      }
      if (noteData.from_user == 'me') {
        delete noteData.from_user;
      }
      const formData = new FormData();
      Object.keys(noteData).forEach((k: string) => {
        if (typeof noteData[k] == 'string') {
          formData.append(k, noteData[k]);
        } else {
          formData.append(k, JSON.stringify(noteData[k]));
        }
      });
      this.files.forEach((f) => {
        formData.append('files', f, f.name);
      });
      if (noteData.uuid) {
        this.noteService.changeNote(noteData.uuid, formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saving = false))
          .subscribe((data) => {
            this.edited.emit(data);
          });
      } else {
        this.noteService.createNote(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saving = false))
          .subscribe((data: Note) => {
            this.created.emit(data);
          });
      }
    }
  }

  deleteFile(index: number, $event: MouseEvent): void {

  }
}
