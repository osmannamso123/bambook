import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {UploadPhotoModal} from '../../ui-kit/modals/upload-photo/upload-photo-modal.component';
import {take, takeUntil} from 'rxjs/operators';
import {NO_AVATAR} from '../../../values/values';
import {UiWindowService} from '../../../services/ui-window.service';
import {UserService} from '../../../services/user.service';
import {UserBaseInfo} from '../../../shared/interfaces/user-base-info';
import {ActivatedRoute} from '@angular/router';
import {NotePagination} from '../../../shared/interfaces/note-pagination';
import {NoteService} from '../../../services/note.service';
import {SameProfileComponent} from '../../../shared/components/same-profile.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends SameProfileComponent implements OnInit, OnDestroy {
  userProfile: UserBaseInfo;
  currentStatus: 'me' | 'friend' | 'request' | 'add_friend';
  friends: Array<UserBaseInfo>;
  notePagination: NotePagination;

  constructor(
    private userService: UserService,
    private uiWindow: UiWindowService,
    private modal: MatDialog,
    private route: ActivatedRoute,
    private noteService: NoteService
  ) {
    super();
  }

  ngOnInit(): void {
    this.routeParams();
    this.getNotes();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
    if (this.bambookTimeout) {
      clearInterval(this.bambookTimeout);
      this.bambookTimeout = null;
    }
  }

  routeParams(): void {
    this.userProfile = null;
    this.route.params
      .pipe(takeUntil(this.destroyed$))
      .subscribe((params: {uuid: string}) => {
        if (params.uuid) {
          this.getSomeoneBaseInfo(params.uuid);
        } else {
          this.getBaseInfo();
        }
      });
  }

  getNotes(): void {
    this.noteService.getNotes(0, {}, 3)
      .pipe(take(1))
      .subscribe((data) => {
        this.notePagination = data;
      });
  }

  getSomeoneBaseInfo(uuid: string): void {
    this.userService.getSomeoneBaseInfo(uuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        if (!data.avatar) {
          data.avatar = NO_AVATAR;
        }
        if (data.is_me) {
          this.fillBambook();
          this.currentStatus = 'me';
        } else if (data.is_friend) {
          this.currentStatus = 'friend';
        } else if (data.sent_request) {
          this.currentStatus = 'request';
        } else {
          this.currentStatus = 'add_friend';
        }
        this.friends = data.friends;
        this.userProfile = data;
      });
  }

  getBaseInfo(): void {
    this.currentStatus = 'me';
    this.userService.userProfile
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        if (data) {
          if (!data.avatar) {
            data.avatar = NO_AVATAR;
          }
          this.friends = data.friends;
          data.is_me = true;
          this.userProfile = data;
          this.fillBambook();
        }
      });
  }

  changeAvatar(): void {
    const uploadModal = this.modal.open(UploadPhotoModal);
    uploadModal.componentInstance.savePhoto
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.loadingAvatar = true;
        uploadModal.close();
        let formData = new FormData();
        formData.append('avatar', data, data.name);
        this.userService.saveAvatar(formData)
          .pipe(take(1))
          .subscribe((response) => {
            this.uiWindow.showSnackBar({
              delay: 2000,
              message: 'Avatar updated',
              purpose: 'confirm'
            });
            // TODO: it updates value, but look at how it does this, bad experience I think
            this.userProfile.avatar = response.avatar;
          });
      });
  }

  addToFriends(): void {
    this.userService.addToFriend(this.userProfile.uuid)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.currentStatus = 'request';
      });
  }

  erroredAvatar() {
    if (this.userProfile) {
      this.userProfile.avatar = NO_AVATAR;
    }
  }
}
