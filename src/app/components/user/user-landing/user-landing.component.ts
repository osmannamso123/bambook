import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserProfile} from '../../../shared/interfaces/user-profile';
import {takeUntil} from 'rxjs/operators';
import * as moment from 'moment';
import {HIGHER_ENUM, NO_AVATAR, VIEW_ENUM} from '../../../values/values';
import {UiWindowService} from '../../../services/ui-window.service';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {UploadPhotoModal} from '../../ui-kit/modals/upload-photo/upload-photo-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {DestroyComponent} from '../../../shared/components/destroy.component';
import {FormMethods} from '../../../shared/classes/form-methods';

@Component({
  selector: 'app-user-landing',
  templateUrl: './user-landing.component.html',
  styleUrls: ['./user-landing.component.scss']
})
export class UserLandingComponent extends DestroyComponent implements OnInit {
  userProfile: UserProfile;
  higherEnum = HIGHER_ENUM;
  viewEnum = VIEW_ENUM;
  isAuth: boolean;
  backgroundImage: string;

  constructor(
    private uiWindow: UiWindowService,
    private userService: UserService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private modal: MatDialog
  ) {
    super();
  }

  ngOnInit(): void {
    this.getDataWithRouteParams();
    this.isAuth = this.authService.isAuthenticated();
    if (this.isAuth) {
      // TODO: look at this code it runs every time
      this.userService.getUserData();
    }
  }

  getDataWithRouteParams(): void {
    this.route.params
      .pipe(takeUntil(this.destroyed$))
      .subscribe((params) => {
        const uuid = params.uuid;
        this.userService.getOverallProfile(uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((data) => {
            data.jobs.forEach((job) => {
              // TODO: format from backend
              if (job.from_date || job.to_date) {
                job.from_date = FormMethods.formatDate(job.from_date);
                job.to_date = FormMethods.formatDate(job.to_date);
              }
            });
            data.schools.forEach((school) => {
              if (school.issue_date) {
                school.issue_date = FormMethods.formatDate(school.issue_date);
              }
            });
            data.institutions.forEach((institution) => {
              if (institution.issue_date) {
                institution.issue_date = FormMethods.formatDate(institution.issue_date);
              }
            });
            data.armies.forEach((army) => {
              if (army.from_date || army.to_date) {
                army.from_date = FormMethods.formatDate(army.from_date);
                army.to_date = FormMethods.formatDate(army.to_date);
              }
            });
            data.interestsList = data.interests?.interests?.split(', ');
            data.activitiesList = data.interests?.activities?.split(', ');
            data.favouriteMusicList = data.interests?.favorite_musics?.split(', ');
            data.favouriteMoviesList = data.interests?.favorite_movies?.split(', ');
            data.favouriteBooksList = data.interests?.favorite_books?.split(', ');
            this.backgroundImage = data.background_image ? `url(${data.background_image})` : null;
            if (!data.avatar) {
              data.avatar = NO_AVATAR;
            }
            this.userProfile = data;
          });
      });
  }

  copyLink(): void {
    // TODO: copy link
    this.uiWindow.showSnackBar({
      delay: 1000,
      message: 'Link copied (Test)',
      purpose: 'confirm'
    });
  }

  changeBackground(): void {
    const uploadModal = this.modal.open(UploadPhotoModal);
    uploadModal.componentInstance.savePhoto
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.userProfile.background_image = null;
        uploadModal.close();
        let formData = new FormData();
        formData.append('background_image', data, data.name);
        this.userService.saveBackground(formData)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((response) => {
            this.uiWindow.showSnackBar({
              delay: 2000,
              message: 'Background updated',
              purpose: 'confirm'
            });
            this.backgroundImage = `url(${response.background_image})`;
          });
      });
  }
}
