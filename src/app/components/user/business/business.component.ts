import {Component, OnInit} from '@angular/core';
import {Company} from '../../../shared/interfaces/company';
import {BusinessService} from '../../../services/business.service';
import {ActivatedRoute} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {SameProfileComponent} from '../../../shared/components/same-profile.component';
import {NO_AVATAR} from '../../../values/values';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.scss']
})
export class BusinessComponent extends SameProfileComponent implements OnInit {
  company: Company;

  constructor(
    private businessService: BusinessService,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.routeParams();
  }

  routeParams(): void {
    this.route.params
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.businessService.getCompany(data.uuid)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((company) => {
            if (!company.logo) {
              company.logo = NO_AVATAR;
            }
            this.company = company;
            this.fillBambook();
          });
      });
  }

  erroredAvatar() {
    if (this.company) {
      this.company.logo = NO_AVATAR;
    }
  }
}
