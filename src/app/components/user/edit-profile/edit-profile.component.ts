import {Component, OnInit} from '@angular/core';
import {GENDERS, HIGHER_STATUSES, REGEX_TO_ADD_HTTP, VIEW_OPTIONS} from '../../../values/values';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {ProfileService} from '../../../services/profile.service';
import {Subscription} from 'rxjs';
import {finalize, takeUntil} from 'rxjs/operators';
import {SearchService} from '../../../services/search.service';
import {Career} from '../../../shared/interfaces/career';
import {HigherEducation} from '../../../shared/interfaces/higher-education';
import {SecondaryEducation} from '../../../shared/interfaces/secondary-education';
import {MilitaryService} from '../../../shared/interfaces/military-service';
import {UiWindowService} from '../../../services/ui-window.service';
import {WarningMessageComponent} from '../../ui-kit/modals/warning-message/warning-message.component';
import {MatDialog} from '@angular/material/dialog';
import {UserService} from '../../../services/user.service';
import {Day} from '../../../shared/models/day';
import {DestroyComponent} from '../../../shared/components/destroy.component';
import {FormMethods} from '../../../shared/classes/form-methods';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent extends DestroyComponent implements OnInit {
  countries = [];
  cities = [];
  genders = GENDERS;
  viewOptions = VIEW_OPTIONS;
  currentState: number;
  formGroup: FormGroup | FormArray;
  higherEducationStatuses = HIGHER_STATUSES;
  saveLoading = false;
  loadingSubscription: Subscription;
  today: Day;

  constructor(
    private profileService: ProfileService,
    private searchService: SearchService,
    private uiWindow: UiWindowService,
    private dialog: MatDialog,
    private userService: UserService
  ) {
    super();
  }

  ngOnInit(): void {
    this.selectState(1);
    const date = new Date();
    this.today = new Day(date.getDate(), date.getMonth(), date.getFullYear());
  }

  selectState(state: number) {
    this.formGroup = null;
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
    this.currentState = state;
    if (state == 1) {
      this.loadingSubscription = this.profileService.getBasicInfo()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.formGroup = this.profileService.getBasicInfoForm(data);
        });
    } else if (state == 2) {
      this.loadingSubscription = this.profileService.getContactInfo()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          if (data.city) {
            this.cities = [data.city];
          }
          if (data.country) {
            this.countries = [data.country];
          }
          this.formGroup = this.profileService.getContactInfoForm(data);
        });
    } else if (state == 3) {
      this.loadingSubscription = this.profileService.getInterests()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.formGroup = this.profileService.getInterestsForm(data);
        });
    } else if (state == 4) {
      this.loadingSubscription = this.profileService.getSecondaryEducation()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.setSecondaryEducationData(data);
        })
    } else if (state == 5) {
      this.loadingSubscription = this.profileService.getHigherEducation()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.setHigherEducationData(data);
        })
    } else if (state == 6) {
      this.loadingSubscription = this.profileService.getCareer()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.setCareerData(data);
        })
    } else if (state == 7) {
      this.loadingSubscription = this.profileService.getMilitaryService()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.setMilitaryServiceData(data);
        })
    } else if (state == 8) {
      this.loadingSubscription = this.profileService.getLifePosition()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.formGroup = this.profileService.getLifePositionForm(data);
        })
    } else if (state == 9) {
      this.loadingSubscription = this.profileService.getPortfolio()
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          this.formGroup = this.profileService.getPortfolioForm(data);
        })
    }
  }

  setHigherEducationData(data: Array<HigherEducation>): void {
    data.forEach((d) => {
      if (d.faculty?.institution?.city) {
        if (this.cities.findIndex((c) => c.uuid == d.faculty.institution.city.uuid) == -1) {
          this.cities.push(d.faculty.institution.city);
        }
      }
    });
    data.forEach((d) => {
      if (d.faculty?.institution?.country) {
        if (this.countries.findIndex((c) => c.uuid == d.faculty.institution.country.uuid) == -1) {
          this.countries.push(d.faculty.institution.country);
        }
      }
    });
    this.formGroup = this.profileService.getHigherEducationForm(data);
  }

  setSecondaryEducationData(data: Array<SecondaryEducation>): void {
    data.forEach((d) => {
      if (d.specialization?.school?.city) {
        if (this.cities.findIndex((c) => c.uuid == d.specialization.school.city.uuid) == -1) {
          this.cities.push(d.specialization.school.city);
        }
      }
    });
    data.forEach((d) => {
      if (d.specialization?.school?.country) {
        if (this.countries.findIndex((c) => c.uuid == d.specialization.school.country.uuid) == -1) {
          this.countries.push(d.specialization.school.country);
        }
      }
    });
    this.formGroup = this.profileService.getSecondaryEducationForm(data);
  }

  setCareerData(data: Array<Career>): void {
    data.forEach((d) => {
      if (d.job?.city) {
        if (this.cities.findIndex((c) => c.uuid == d.job.city.uuid) == -1) {
          this.cities.push(d.job.city);
        }
      }
    });
    data.forEach((d) => {
      if (d.job?.country) {
        if (this.countries.findIndex((c) => c.uuid == d.job.country.uuid) == -1) {
          this.countries.push(d.job.country);
        }
      }
    });
    this.formGroup = this.profileService.getCareerForm(data);
  }

  setMilitaryServiceData(data: Array<MilitaryService>): void {
    data.forEach((d) => {
      if (d.army_part?.country) {
        if (this.countries.findIndex((c) => c.uuid == d.army_part.country.uuid) == -1) {
          this.countries.push(d.army_part.country);
        }
      }
    });
    this.formGroup = this.profileService.getMilitaryForm(data);
  }

  addAnotherSchool(): void {
    (this.formGroup as FormArray).push(this.profileService.addSecondaryEducationForm());
  }

  addAnotherUniversity(): void {
    (this.formGroup as FormArray).push(this.profileService.addHigherEducationForm());
  }

  addAnotherCareer(): void {
    (this.formGroup as FormArray).push(this.profileService.addCareerForm());
  }

  addAnotherMilitaryService(): void {
    (this.formGroup as FormArray).push(this.profileService.addMilitaryForm());
  }

  addAnotherPortfolio(): void {
    (this.formGroup as FormArray).push(this.profileService.addPortfolioForm());
  }

  deleteForm(i: any): void {
    if ((this.formGroup as FormArray).at(i).get('uuid')) {
      if (this.currentState == 4) {
        this.profileService.deleteSecondaryEducation((this.formGroup as FormArray).at(i).get('uuid').value)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            (this.formGroup as FormArray).removeAt(i);
          });
      } else if (this.currentState == 5) {
        this.profileService.deleteHigherEducation((this.formGroup as FormArray).at(i).get('uuid').value)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            (this.formGroup as FormArray).removeAt(i);
          });
      } else if (this.currentState == 6) {
        this.profileService.deleteCareer((this.formGroup as FormArray).at(i).get('uuid').value)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            (this.formGroup as FormArray).removeAt(i);
          });
      } else if (this.currentState == 7) {
        this.profileService.deleteMilitaryService((this.formGroup as FormArray).at(i).get('uuid').value)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            (this.formGroup as FormArray).removeAt(i);
          });
      } else if (this.currentState == 9) {
        this.profileService.deletePortfolio((this.formGroup as FormArray).at(i).get('uuid').value)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            (this.formGroup as FormArray).removeAt(i);
          });
      }
    }
    else {
      (this.formGroup as FormArray).removeAt(i);
    }
  }

  snackBarSaved(): void {
    this.uiWindow.showSnackBar({
      delay: 1000,
      message: 'Saved',
      purpose: 'confirm'
    });
  }

  getFormattedFormData(formData: any): any {
    if (this.formGroup instanceof FormArray) {
      formData.forEach((fb) => {
        Object.keys(fb).forEach((key: string) => {
          if (fb[key] == null || fb[key] == '') {
            delete fb[key];
          }
        });
      });
    } else {
      Object.keys(formData).forEach((key: string) => {
        if (formData[key] == null || formData[key] == '') {
          delete formData[key];
        }
      });
    }

    return formData;
  }

  saveForm(): void {
    if (this.currentState == 2) {
      (this.formGroup as FormGroup).get('website').setValue(FormMethods.addHttp((this.formGroup as FormGroup).get('website').value));
    } else if (this.currentState == 9) {
      (this.formGroup as FormArray).controls.forEach((fb: FormGroup) => {
        fb.get('url').setValue(FormMethods.addHttp(fb.get('url').value));
      });
    }
    this.formGroup.markAllAsTouched();
    if (this.formGroup.valid) {
      this.saveLoading = true;
      let formData = this.getFormattedFormData(this.formGroup.getRawValue());
      if (this.currentState == 1) {
        this.profileService.saveBasicInfo(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe(() => {
            this.userService.getUserData();
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 2) {
        this.profileService.saveContactInfo(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe(() => {
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 3) {
        this.profileService.saveInterests(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe(() => {
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 4) {
        this.profileService.saveSecondaryEducation(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe((data) => {
            data.forEach((d, i) => {
              if (!(this.formGroup as FormArray).at(i).get('uuid')) {
                ((this.formGroup as FormArray).controls[i] as FormGroup).controls.uuid = new FormControl(d.uuid);
              }
            });
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 5) {
        this.profileService.saveHigherEducation(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe((data) => {
            data.forEach((d, i) => {
              if (!(this.formGroup as FormArray).at(i).get('uuid')) {
                ((this.formGroup as FormArray).controls[i] as FormGroup).controls.uuid = new FormControl(d.uuid);
              }
            });
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 6) {
        this.profileService.saveCareer(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe((data) => {
            data.forEach((d, i) => {
              if (!(this.formGroup as FormArray).at(i).get('uuid')) {
                ((this.formGroup as FormArray).controls[i] as FormGroup).controls.uuid = new FormControl(d.uuid);
              }
            });
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 7) {
        this.profileService.saveMilitaryService(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe((data) => {
            data.forEach((d, i) => {
              if (!(this.formGroup as FormArray).at(i).get('uuid')) {
                ((this.formGroup as FormArray).controls[i] as FormGroup).controls.uuid = new FormControl(d.uuid);
              }
            });
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 8) {
        this.profileService.saveLifePosition(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe(() => {
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      } else if (this.currentState == 9) {
        this.profileService.savePortfolio(formData)
          .pipe(takeUntil(this.destroyed$), finalize(() => this.saveLoading = false))
          .subscribe((data) => {
            data.forEach((d, i) => {
              if (!(this.formGroup as FormArray).at(i).get('uuid')) {
                ((this.formGroup as FormArray).controls[i] as FormGroup).controls.uuid = new FormControl(d.uuid);
              }
            });
            this.snackBarSaved();
          }, () => this.showErrorSnackBar());
      }
    }
  } // TODO: reset form with cities and arrays

  showErrorSnackBar(): void {
    this.dialog.open(WarningMessageComponent, {
      data: {
        message: 'Some errors occurred while processing your request'
      }
    });
  }

  searchCountry(word: string): void {
    this.searchService.searchCountries(word)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.countries = data;
      });
  }

  searchCity(word: string): void {
    this.searchService.searchCities(word, this.formGroup.get('country').value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.cities = data;
      });
  }

  searchCityWithFormGroup(word: string, formGroup: FormGroup): void {
    this.searchService.searchCities(word, formGroup.get('country').value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.cities = data;
      });
  }

  resetForm(): void {
    // TODO: not loading all data from get request, storing in gotData
    this.selectState(this.currentState);
  }
}
