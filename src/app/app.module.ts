import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ClientModule} from './modules/client/client.module';
import {AppRoutingModule} from './app-routing.module';
import {UserModule} from './modules/user/user.module';
import {UserComponent} from './modules/user/user.component';
import {ClientComponent} from './modules/client/client.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {IConfig, NgxMaskModule} from 'ngx-mask';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthGuard} from './shared/guards/auth-guard';
import {AuthInterceptor} from './shared/interceptors/auth-interceptor';
import {UIKitModule} from './modules/ui-kit.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    ClientComponent
  ],
  imports: [
    BrowserModule,
    ClientModule,
    AppRoutingModule,
    HttpClientModule,
    UserModule,
    FormsModule,
    ReactiveFormsModule,
    UIKitModule,
    NgxMaskModule.forRoot(maskConfig),
    NoopAnimationsModule,
    MatDialogModule
  ],
  exports: [
    NgxMaskModule
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
