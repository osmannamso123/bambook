import {Injectable, OnDestroy} from '@angular/core';
import {ReplaySubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DestroyComponent implements OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  constructor() {}

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
