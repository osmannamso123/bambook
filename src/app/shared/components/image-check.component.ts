import {WarningMessageComponent} from '../../components/ui-kit/modals/warning-message/warning-message.component';
import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DestroyComponent} from './destroy.component';

@Injectable({
  providedIn: 'root'
})
export class ImageCheckComponent extends DestroyComponent {
  allowedFormats: Array<string> = [];

  constructor(
    public dialog: MatDialog
  ) {
    super();
  }

  checkForFormat(files: Array<File>): boolean {
    if (this.allowedFormats.length) {
      if (files.find((f) => this.allowedFormats.indexOf(f.name.split('.').pop()) == -1)) {
        this.dialog.open(WarningMessageComponent, {
          data: {
            message: `File format is not allowed, use one of this: ${this.allowedFormats.join(', ')}`
          }
        });
        return false;
      }
    }

    return true;
  }
}
