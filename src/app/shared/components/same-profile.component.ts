import {Injectable} from '@angular/core';
import {NO_AVATAR, NO_AVATAR_SMALL} from '../../values/values';
import {DestroyComponent} from './destroy.component';

@Injectable({
  providedIn: 'root'
})
export class SameProfileComponent extends DestroyComponent {
  loadingAvatar = true;
  noAvatar = NO_AVATAR;
  noAvatarSmall = NO_AVATAR_SMALL;
  percentage = 0;
  bambookTimeout: number;

  constructor() {
    super();
  }

  fillBambook(): void {
    this.bambookTimeout = setTimeout(() => {
      const progressFill = document.getElementById('progress-fill');
      const progressPercentage = document.getElementById('progress-percentage');
      progressFill.style.height = '100%';
      const interval = setInterval(() => {
        this.percentage += 1;
        if (this.percentage >= 40 && this.percentage < 75) {
          progressFill.classList.add('medium-color');
          progressPercentage.classList.add('medium-color');
        }
        if (this.percentage >= 75 && this.percentage <= 100) {
          progressFill.classList.add('high-color');
          progressPercentage.classList.add('high-color');
        }
        if (this.percentage >= 100) {
          this.percentage = 100;
          clearInterval(interval);
        }
      }, 10);
    }, 100);
  }

  avatarLoaded(): void {
    this.loadingAvatar = false;
  }
}
