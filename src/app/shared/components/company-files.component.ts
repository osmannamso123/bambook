import {Injectable, OnInit} from '@angular/core';
import {ImageCheckComponent} from './image-check.component';
import {MatDialog} from '@angular/material/dialog';
import {CompanyFile} from '../interfaces/company-file';
import {FILE_EXTENSIONS} from '../../values/values';

@Injectable({
  providedIn: 'root'
})
export abstract class CompanyFilesComponent extends ImageCheckComponent implements OnInit {
  companyUuid: string;
  isAdding = false;
  files: Array<CompanyFile> = [];
  fileExtensions = FILE_EXTENSIONS;

  abstract getFiles(): void;
  abstract uploadFiles(files: Array<File>): void;
  abstract deleteFile(uuid: string): void;

  protected constructor(
    public dialog: MatDialog
  ) {
    super(dialog);
  }

  ngOnInit(): void {
    this.getFiles();
  }

  fileDropped(files: Array<File>): void {
    if (this.checkForFormat(files)) {
      this.uploadFiles(files);
    }
  }

  fileChanged(event: Event): void {
    const files: Array<File> = Array.from((event.target as HTMLInputElement).files);
    if (this.checkForFormat(files)) {
      this.uploadFiles(files);
    }
  }

  removeFile(index: number): void {
    this.deleteFile(this.files[index].uuid);
    this.files.splice(index, 1);
  }
}
