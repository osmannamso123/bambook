export class Day {
  date: number;
  month: number;
  year: number;
  selected?: boolean;
  disabled?: boolean;

  constructor(date, month, year) {
    this.date = date;
    this.month = month;
    this.year = year;
  }

  isMore(day: Day): boolean {
    if (this.year > day.year) {
      return true;
    } else if (this.year == day.year) {
      if (this.month > day.month) {
        return true;
      } else if (this.month == day.month) {
        if (this.date > day.date) {
          return true;
        }
      }
    }

    return false;
  }
}
