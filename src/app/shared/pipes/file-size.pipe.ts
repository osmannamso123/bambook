import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {
  transform(value: number, ...args): string {
    if (value < 1000) {
      return `${value} b`;
    } else if (value < 1000000) {
      return `${Math.round(value/1000)} kb`;
    } else if (value < 1000000000) {
      return `${Math.round(value/1000000)} Mb`;
    }
  }
}
