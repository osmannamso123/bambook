import {FormGroup} from '@angular/forms';
import {REGEX_TO_ADD_HTTP} from '../../values/values';
import * as moment from 'moment';
import {Day} from '../models/day';

export class FormMethods {
  static emptyForm(form: FormGroup): FormGroup {
    Object.keys(form.controls).forEach((key) => {
      if (!form.get(key).value) {
        form.removeControl(key);
      }
    });

    return form;
  }

  static addHttp(url): string {
    if (!REGEX_TO_ADD_HTTP.test(url)) {
      url = "http://" + url;
    }
    return url;
  }

  static formatDate(date: string): string {
    const formattedDate = moment({day: +date.substring(0, 2), month: (+date.substring(2, 4) - 1), year: +date.substring(4, 8)});
    return formattedDate.format('D MMM YYYY');
  }

  static formatSplitDate(date: string): Day {
    return new Day(+date.substring(0, 2), (+date.substring(2, 4) - 1), +date.substring(4, 8));
  }

  static formatDashDate(date: string): string {
    return date.substring(8, 10) + date.substring(5, 7) + date.substring(0, 4);
  }
}
