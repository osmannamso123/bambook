import {CompanyFile} from '../interfaces/company-file';

export class ObjectMutates {
  static addFileExtensions(files: Array<CompanyFile>): Array<any> {
    return files.map((f) => {
      f.ext = f.filename.split('.').pop();
      return f;
    });
  }
}
