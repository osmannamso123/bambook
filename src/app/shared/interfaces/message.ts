import {UserBaseInfo} from './user-base-info';

export interface Message {
  text: string;
  command: 'new_message';
  author: UserBaseInfo;
  created_at: string;
  is_me?: boolean;
}
