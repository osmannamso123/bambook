export interface CompanyWorker {
  worker: string;
  document: string;
  filename: string;
  uuid: string;
}
