import {School} from './school';

export interface Specialization {
  name: string;
  uuid: string;
  school: School;
}
