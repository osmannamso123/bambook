export interface CompanySupplier {
  supplier: string;
  contact: string;
  document: string;
  filename: string;
  uuid: string;
}
