import {City} from '../models/city';
import {Country} from '../models/country';
import {Company} from './company';

export interface UserBaseInfo {
  first_name: string;
  last_name: string;
  city: City;
  avatar: string;
  country: Country;
  email: string;
  phone: string;
  uuid: string;
  is_me: boolean;
  is_friend: boolean;
  sent_request: boolean;
  friends: Array<UserBaseInfo>;
  companies: Array<Company>;
}
