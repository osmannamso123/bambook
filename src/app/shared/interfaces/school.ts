import {City} from '../models/city';
import {Country} from '../models/country';

export interface School {
  name: string;
  uuid: string;
  city: City;
  country: Country;
}
