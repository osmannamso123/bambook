export interface NoteFilter {
  filtering?: string;
  sorting?: string;
  to_date?: string;
  from_date?: string;
}
