import {City} from '../models/city';
import {Country} from '../models/country';
import {Specialization} from './specialization';

export interface SecondaryEducation {
  uuid: string;
  country: Country;
  city: City;
  school: string;
  issue_date: string;
  specialization_name: string;
  specialization: Specialization;
}
