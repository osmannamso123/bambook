import {Country} from '../models/country';
import {City} from '../models/city';

export interface Institution {
  institution: string;
  city: City;
  country: Country
}
