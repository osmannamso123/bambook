export interface FriendRequest {
  uuid: string;
  phone: string;
  first_name: string;
  last_name: string;
  message: string;
  created: string;
}
