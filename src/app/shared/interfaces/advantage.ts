export interface Advantage {
  name: string;
  image: string;
  text: string;
}
