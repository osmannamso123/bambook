import {City} from '../models/city';
import {Country} from '../models/country';

export interface Job {
  city: City;
  country: Country;
  name: string;
}
