export interface Interest {
  activities: string;
  interests: string;
  favorite_musics: string;
  favorite_movies: string;
  favorite_books: string;
  favorite_quotes: string;
  about_me: string;
}
