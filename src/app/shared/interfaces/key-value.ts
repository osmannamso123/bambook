export interface KeyValue {
  name: string;
  uuid: string | number;
}
