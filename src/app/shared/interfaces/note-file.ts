export interface NoteFile {
  file: string;
  filename: string;
  size: number;
}
