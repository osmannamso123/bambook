export interface CompanyClient {
  client: string;
  document: string;
  filename: string;
  uuid: string;
}
