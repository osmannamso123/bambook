import {City} from '../models/city';
import {Country} from '../models/country';

export interface CompanyAbout {
  website: string;
  phone: string;
  additional_phone: string;
  fax: string;
  iban: string;
  type_of_business: string;
  country: Country;
  city: City;
  street: string;
  post_address: string;
  working_days: string;
  working_time: string;
}
