import {ArmyPart} from './army-part';

export interface MilitaryService {
  uuid: string;
  army_part: ArmyPart;
  from_date: string;
  to_date: string;
}
