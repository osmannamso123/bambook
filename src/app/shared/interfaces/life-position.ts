export interface LifePosition {
  political_views: number;
  world_views: number;
  personal_priority: number;
  important_in_others: number;
  smoking_views: number;
  alcohol_views: number;
  inspired_by: number;
}
