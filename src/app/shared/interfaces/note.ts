import {UserBaseInfo} from './user-base-info';
import {NoteFile} from './note-file';

export interface Note {
  deadline: string;
  from_company: string;
  from_user: UserBaseInfo;
  is_request: boolean;
  priority: number;
  tag: string;
  tags: Array<string>;
  text: string;
  uuid: string;
  name: string;
  created_at: string;
  files: Array<NoteFile>;
  chat: boolean;
  chat_uuid: string;
}
