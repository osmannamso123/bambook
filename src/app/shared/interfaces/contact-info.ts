import {City} from '../models/city';
import {Country} from '../models/country';

export interface ContactInfo {
  country: Country;
  city: City;
  email: string;
  alt_phone: string;
  skype: string;
  website: string;
  instagram: string;
  facebook: string;
}
