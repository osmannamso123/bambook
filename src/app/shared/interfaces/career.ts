import {Job} from './job';

export interface Career {
  uuid: string;
  company: string;
  job: Job;
  from_date: string;
  to_date: string;
  position: string;
}
