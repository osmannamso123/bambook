import {Message} from './message';

export interface ChatResponse {
  messages?: Array<Message>;
  message?: Message;
  type?: 'send_message' | 'new_message';
  command?: 'send_message' | 'new_message';
}
