export interface BasicInfo {
  username: string;
  first_name: string;
  last_name: string;
  gender: string;
  birth_date: string;
  position: string;
}
