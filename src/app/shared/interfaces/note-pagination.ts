import {Note} from './note';

export interface NotePagination {
  count: number;
  next: boolean;
  previous: string;
  results: Array<Note>;
}
