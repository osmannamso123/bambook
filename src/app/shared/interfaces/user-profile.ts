import {ContactInfo} from './contact-info';
import {Interest} from './interest';
import {SecondaryEducation} from './secondary-education';
import {HigherEducation} from './higher-education';
import {Career} from './career';
import {MilitaryService} from './military-service';
import {LifePosition} from './life-position';
import {Portfolio} from './portfolio';

export interface UserProfile {
  uuid: string;
  phone: string;
  username: string;
  first_name: string;
  position: string;
  last_name: string;
  birth_date: string;
  background_image: string;
  avatar: string;
  gender: string;
  contacts: ContactInfo;
  interests: Interest;
  schools: Array<SecondaryEducation>;
  institutions: Array<HigherEducation>;
  jobs: Array<Career>;
  armies: Array<MilitaryService>;
  life_position: LifePosition;
  portfolios: Array<Portfolio>;
  is_me: boolean;
  is_friend: boolean;
  interestsList: Array<string>;
  activitiesList: Array<string>;
  favouriteMusicList: Array<string>;
  favouriteMoviesList: Array<string>;
  favouriteBooksList: Array<string>;
}
