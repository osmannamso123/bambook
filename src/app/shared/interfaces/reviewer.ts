export interface Reviewer {
  name: string;
  work: string;
  avatar: string;
  quote: string;
}
