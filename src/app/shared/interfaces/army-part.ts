import {Country} from '../models/country';

export interface ArmyPart {
  country: Country;
  name: string;
}
