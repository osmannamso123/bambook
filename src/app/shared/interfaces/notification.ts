import {Note} from './note';
import {UserBaseInfo} from './user-base-info';

export interface Notification {
  actor: Note | UserBaseInfo;
  unread: boolean;
  verb: string;
}
