import {Institution} from './institution';

export interface Faculty {
  institution: Institution;
  name: string;
}
