export interface CompanyFile {
  uuid: string;
  file?: string;
  image?: string;
  video?: string;
  document?: string;
  filename: string;
  ext: string;
}
