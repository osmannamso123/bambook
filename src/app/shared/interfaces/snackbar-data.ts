export interface SnackbarData {
  message: string;
  purpose: 'confirm' | 'warning';
  delay: number;
}
