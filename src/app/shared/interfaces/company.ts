export interface Company {
  uuid: string;
  name: string;
  logo: string;
  slogan: string;
}
