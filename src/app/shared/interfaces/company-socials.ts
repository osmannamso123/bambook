export interface CompanySocials {
  facebook: string;
  whatsapp: string;
  instagram: string;
  linkedin: string;
  piscart: string;
  skype: string;
  viber: string;
  telegram: string;
  wechat: string;
  vk: string;
  google: string;
  youtube: string;
}
