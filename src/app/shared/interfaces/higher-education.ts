import {City} from '../models/city';
import {Country} from '../models/country';
import {Faculty} from './faculty';

export interface HigherEducation {
  uuid: string;
  country: Country;
  city: City;
  faculty: Faculty;
  faculty_name: string;
  institution: string;
  form_training: string;
  issue_date: string;
  status: string;
}
