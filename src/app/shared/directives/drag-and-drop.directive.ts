import {Directive, EventEmitter, HostBinding, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appDragDrop]'
})
export class DragDropDirective {
  @Output() onFileDropped = new EventEmitter<Array<File>>();
  @HostBinding('class') elementClass = 'no-drop';

  @HostListener('dragover', ['$event']) onDragOver(evt): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.elementClass = 'yes-drop';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.elementClass = 'no-drop';
  }

  @HostListener('drop', ['$event']) public ondrop(evt): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.elementClass = 'no-drop';
    let files: Array<File> = Array.from(evt.dataTransfer.files);
    if (files.length > 0) {
      this.onFileDropped.emit(files as Array<File>);
    }
  }
}
